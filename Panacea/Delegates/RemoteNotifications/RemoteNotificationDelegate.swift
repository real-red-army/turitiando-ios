//
//  RemoteNotificationDelegate.swift
//  Panacea
//
//  Created by Luis Romero on 10/12/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Firebase

class RemoteNotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
  func registerPushNotification(_ application: UIApplication) {
    /// For iOS 10 display notification (sent via APNS)
    UNUserNotificationCenter.current().delegate = self

    let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]

    UNUserNotificationCenter.current().requestAuthorization(
        options: authOptions,
        completionHandler: {_, _ in })

  }

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
                              withCompletionHandler
                              completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let content = notification.request.content.userInfo

    saveRemoteNotification(content)

    completionHandler([.alert, .badge, .sound])
  }

  /// After user tapped the notification this method is called
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    let content = response.notification.request.content.userInfo

    saveRemoteNotification(content)

    completionHandler()
  }

  func saveRemoteNotification(_ content: [AnyHashable: Any]) {
    let notificationType = content["item_type"] as? String ?? "NotFound"
    let entityId = content["item_id"] as? Double ?? 0
    let title = content["title"] as? String ?? ""
    let details = content["details"] as? String ?? ""

    let remoteNotification = RemoteNotification(0,
                                                title: title,
                                                details: details,
                                                notificationType:notificationType,
                                                entityId: entityId)

    let dataAccess = DataAccess<RemoteNotification>()
    dataAccess.saveItem(remoteNotification)
  }

  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    /// When the notifications of this code worked well, there was not yet.
    Messaging.messaging().apnsToken = deviceToken
  }
}
