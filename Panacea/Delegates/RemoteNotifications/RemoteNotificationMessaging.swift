//
//  RemoteNotificationMessaging.swift
//  Panacea
//
//  Created by Luis Romero on 10/12/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Firebase

class RemoteNotificationMessaging: NSObject, MessagingDelegate {
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken token: String) {
    let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString ?? ""

    _ = RemoteNotificationDataHandler
      .shared
      .signupForNotification(withDevice: deviceIdentifier, token: token) { (error) in
        print("Completion signup for notification %@ \n %@", error ?? "", token)
    }
  }

  func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
    print("Notificacion recibida!")
  }
}
