//
//  TourTileController.swift
//  Panacea
//
//  Created by Blashadow on 6/20/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

enum TourTileStyle {
  case fullWidth
  case customWidth(Int)
}

class TourTileController: TileController {
  static let tourTileHeight: CGFloat = 130
  private var styleWith: TourTileStyle?

  init(with pressHandler: ((DisplayableTile) -> Void)?, style: TourTileStyle) {
    super.init(with: pressHandler)

    self.styleWith = style
  }

  override func sizeForItem(at index: Int) -> CGSize {
    let height = TourTileController.tourTileHeight
    guard let collectionContext = collectionContext,
      let styleWith = self.styleWith else {
        return CGSize(width: 300, height: height)
    }

    switch styleWith {
    case .fullWidth:
      let width = collectionContext.containerSize.width - (horizontalInsects * 2)
      let value = CGSize(width: width, height: height)
      return value
    case .customWidth(let width):
      return CGSize(width: width, height: Int(height))
    }
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let collectionContext = collectionContext else {
      return UICollectionViewCell(frame: .zero)
    }

    let cell = collectionContext.dequeueReusableCell(of: TileTourViewCell.self,
                                                     withReuseIdentifier: "TILE_TOUR",
                                                     for: self,
                                                     at: index)

    guard let tileCell = cell as? TileTourViewCell,
      let tourTitle = tile?.title,
      let imageUrl = tile?.imageHeaderUrl
      else {
        return cell
    }

    let description = tile?.details ?? ""
    let price = tile?.price ?? 0

    tileCell.setupDataCell(title: tourTitle, description:description, imageUrl: imageUrl, price: Int(price))

    return cell
  }
}
