//
//  GenericListSectionController.swift
//  Panacea
//
//  Created by Luis Romero on 11/3/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import IGListKit

class GenericListSectionController<T: AnyObject>: ListSectionController {
  typealias Context = ListCollectionContext
  var item: T?
  var sectionCellDisplayable: SectionCellDisplayable
  var pressHandler: ((_: T) -> Void)

  init(pressHandler handler:@escaping (_: T) -> Void, displayableItem: SectionCellDisplayable) {
    sectionCellDisplayable = displayableItem
    pressHandler = handler
  }

  // MARK: List section methods
  override func numberOfItems() -> Int {
    return 1
  }

  override func sizeForItem(at index: Int) -> CGSize {
    guard let item = item else {
      return CGSize(width: 0, height: 0)
    }

    let contextWidth = collectionContext?.containerSize.width ?? 0

    return sectionCellDisplayable.sizeForItem(at: index, selectedItem: item, contextWidth: contextWidth)
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let context = collectionContext, let selectedItem = item else {
      return UICollectionViewCell(frame: .zero)
    }

    return sectionCellDisplayable.itemCell(context, selectedItem: selectedItem, index: index, sectionController: self)
  }

  override func didUpdate(to object: Any) {
    item = object as? T
  }

  override func didSelectItem(at index: Int) {
    if let image = self.item {
      pressHandler(image)
    }
  }
}
