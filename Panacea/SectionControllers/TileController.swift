//
//  TileController.swift
//  Panacea
//
//  Created by Blashadow on 6/16/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class TileController: ListSectionController {
  var tile: DisplayableTile?
  var pressHandler: ((_: DisplayableTile) -> Void)?
  let horizontalInsects = CGFloat(5)

  init(with pressHandler: ((_: DisplayableTile) -> Void)?) {
    super.init()

    self.pressHandler = pressHandler
    self.inset = UIEdgeInsets(top: 0, left: horizontalInsects, bottom: 0, right: horizontalInsects)
  }

  // MARK: List section methods
  override func numberOfItems() -> Int {
    return 1
  }

  override func sizeForItem(at index: Int) -> CGSize {
    return CGSize(width: 100, height: 100)
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let collectionContext = collectionContext else {
      return UICollectionViewCell(frame: .zero)
    }

    let cell = collectionContext.dequeueReusableCell(of: TilePlaceViewCell.self,
                                    withReuseIdentifier: "TILE",
                                                    for: self,
                                                     at: index)

    guard let tileCell = cell as? TilePlaceViewCell,
      let placeTitle = tile?.title,
      let imageUrl = tile?.imageHeaderUrl
      else {
      return cell
    }

    tileCell.setupDataCell(title: placeTitle, imageUrl: imageUrl)

    return cell
  }

  override func didUpdate(to object: Any) {
    self.tile = object as? DisplayableTile
  }

  override func didSelectItem(at index: Int) {
    if let handler = self.pressHandler, let tile = tile {
      handler(tile)
    }
  }
}
