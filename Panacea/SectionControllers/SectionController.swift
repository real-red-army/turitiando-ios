//
//  SectionController.swift
//  Panacea
//
//  Created by Blashadow on 6/21/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

typealias Context = ListCollectionContext

protocol SectionCellDisplayable {
  /// Return corresponding cell for each item
  func itemCell(_ context: Context,
                selectedItem: AnyObject,
                index: Int,
                sectionController: ListSectionController) -> UICollectionViewCell

  /// Compute size for each collection cell
  func sizeForItem(at index: Int, selectedItem: AnyObject, contextWidth: CGFloat) -> CGSize
}

class SectionController: ListSectionController {
  var sectionItem: SectionItems?
  var sectionCellDisplayable: SectionCellDisplayable?

  init(_ sectionCellDisplayable: SectionCellDisplayable?) {
    super.init()

    self.sectionCellDisplayable = sectionCellDisplayable
  }

  // MARK: List section methods
  override func numberOfItems() -> Int {
    guard let sectionItem = sectionItem else {
      return 0
    }

    let list = sectionItem.itemType.value as? [Any] ?? []

    return list.count + 1
  }

  override func sizeForItem(at index: Int) -> CGSize {
    let contextWidth = collectionContext?.containerSize.width ?? 0

    if index == 0 {
      return CGSize(width: contextWidth, height: 40)
    }

    guard let sectionCellDisplayable = self.sectionCellDisplayable else {
      return CGSize(width: contextWidth, height: 40)
    }

    let value = self.sectionItem?.itemType.value as AnyObject
    return sectionCellDisplayable.sizeForItem(at: index, selectedItem: value, contextWidth: contextWidth)
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let collectionContext = collectionContext else {
      return UICollectionViewCell(frame: .zero)
    }

    if index == 0 {
      return headerCell(context: collectionContext, sectionItem: sectionItem, index: index)
    }

    let value = self.sectionItem?.itemType.value as AnyObject

    if let cellDisplayable = self.sectionCellDisplayable {
      return cellDisplayable.itemCell(collectionContext,
                                      selectedItem: value,
                                      index: index,
                                      sectionController: self)
    }

    return itemCell(context: collectionContext, sectionItem: sectionItem, index: index)
  }

  private func headerCell(context: Context, sectionItem: SectionItems?, index: Int) -> UICollectionViewCell {
    let cell = context.dequeueReusableCell(of: HomeSectionHeaderCollectionViewCell.self,
                                           withReuseIdentifier: "IDENTIIER",
                                           for: self,
                                           at: index)

    guard let cellHeader = cell as? HomeSectionHeaderCollectionViewCell else {
      return UICollectionViewCell(frame: .zero)
    }

    cellHeader.shouldShowViewAllLabel = false
    cellHeader.label.text = sectionItem?.title

    return cellHeader
  }

  private func itemCell(context: Context, sectionItem: SectionItems?, index: Int) -> UICollectionViewCell {
    let cell = context.dequeueReusableCell(of: TourCollectionViewCell.self,
                                           withReuseIdentifier: "TOUR",
                                           for: self,
                                           at: index)

    guard let placeCell = cell as? TourCollectionViewCell,
      let sectionItem = sectionItem,
      let items = sectionItem.itemType.value as? [Place] else {
      return UICollectionViewCell(frame: .zero)
    }

    let tour = items[index - 1]
    guard let title = tour.placeTitle,
      let description = tour.placeDescription,
      let imageUrl = tour.placeImageHeaderUrl else {
      return UICollectionViewCell(frame: .zero)
    }

    let price = tour.placePrice ?? 0

    let dateString = "\(tour.dueDateString())\n(\(tour.remainingDays()))"
    placeCell.setupDataCell(title: title,
                            description: description,
                            imageUrl: imageUrl,
                            price: Int(price),
                            date: dateString)

    // if the section item is 'place' hide the due date
    if case .place(_) = sectionItem.itemType {
      placeCell.tourDate.isHidden = true
    }

    if price < 1 {
      placeCell.tourPrice.isHidden = true
    }

    return placeCell
  }

  override func didUpdate(to object: Any) {
    sectionItem = object as? SectionItems
  }

  override func didSelectItem(at index: Int) {
    guard let sectionItem = sectionItem, let items = sectionItem.itemType.value as? [Any] else {
      return
    }

    if index > 0 && !items.isEmpty {
      let item = items[index - 1]

      if let sectionItem = self.sectionItem,
          let handler = sectionItem.itemPressHandler {
        handler(item)
      }
    }
  }
}
