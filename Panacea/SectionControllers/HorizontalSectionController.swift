//
//  HorizontalSectionController.swift
//  Panacea
//
//  Created by Blashadow on 10/20/18.
//  Copyright © 2018 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class HorizontalSectionController: SectionController {
  override init(_ sectionCellDisplayable: SectionCellDisplayable?) {
    super.init(sectionCellDisplayable)

    self.inset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
  }

  // MARK: List section methods
  override func numberOfItems() -> Int {
    return 2
  }

  override func sizeForItem(at index: Int) -> CGSize {
    let contextWidth = collectionContext?.containerSize.width ?? 0

    if index == 0 {
      return CGSize(width: contextWidth, height: 40)
    }

    let numberOfItemPerRow = UIDevice.current.userInterfaceIdiom == .pad ? 2 : 1
    let itemWidth = contextWidth / CGFloat(numberOfItemPerRow)

    return CGSize(width: itemWidth, height: 150)
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let collectionContext = collectionContext else {
      return UICollectionViewCell(frame: .zero)
    }

    if index == 0 {
      return headerCell(context: collectionContext, sectionItem: sectionItem, index: index)
    }

    return itemCell(context: collectionContext, sectionItem: sectionItem, index: index)
  }

  private func headerCell(context: Context, sectionItem: SectionItems?, index: Int) -> UICollectionViewCell {
    let cell = context.dequeueReusableCell(of: HomeSectionHeaderCollectionViewCell.self,
                                                     withReuseIdentifier: "IDENTIIER",
                                                     for: self,
                                                     at: index)

    guard let cellHeader = cell as? HomeSectionHeaderCollectionViewCell else {
      return UICollectionViewCell(frame: .zero)
    }

    cellHeader.tapViewAllHandler = sectionItem?.headerPressHandler
    cellHeader.label.text = sectionItem?.title

    return cellHeader
  }

  private func itemCell(context: Context, sectionItem: SectionItems?, index: Int) -> UICollectionViewCell {
    let cell = context.dequeueReusableCell(of: CollectionViewHorizontalCell.self,
                                                     withReuseIdentifier: "SECTION1",
                                                     for: self,
                                                     at: index)

    guard let horizontalCell = cell as? CollectionViewHorizontalCell, let sectionItem = sectionItem else {
      return UICollectionViewCell(frame: .zero)
    }

    horizontalCell.setupCell(width: sectionItem)

    return horizontalCell
  }
}
