//
//  ForecastItemDisplayableCell.swift
//  Panacea
//
//  Created by Luis Romero on 11/4/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class ForecastItemDisplayableCell: SectionCellDisplayable {
  func itemCell(_ context: Context,
                selectedItem: AnyObject,
                index: Int,
                sectionController: ListSectionController) -> UICollectionViewCell {
    let cell = context.dequeueReusableCell(of: ForecastCollectionViewCell.self,
                                           withReuseIdentifier: "FORECAST",
                                           for: sectionController,
                                           at: index)

    guard let forecastCell = cell as? ForecastCollectionViewCell,
      let item = selectedItem as? ForecastItem else {
      return cell
    }

    forecastCell.setupCell(with: item)

    return forecastCell
  }

  func sizeForItem(at index: Int, selectedItem: AnyObject, contextWidth: CGFloat) -> CGSize {
    return CGSize(width: contextWidth, height: 75)
  }
}
