//
//  NotificationDisplayableCell.swift
//  Panacea
//
//  Created by Luis Romero on 10/9/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class NotificationDisplayableCell: SectionCellDisplayable {
  func sizeForItem(at index: Int, selectedItem: AnyObject, contextWidth: CGFloat) -> CGSize {
    var titleHeight: CGFloat = 0
    var subTitleHeight: CGFloat = 0
    var detailsHeight: CGFloat = 0
    let margin: CGFloat = 5
    let contentContainerWidth = contextWidth
        - margin * 2
        - NotificationCollectionViewCell.containerMarginHorizontal
        - NotificationCollectionViewCell.iconSize

    if let items = selectedItem as? [RemoteNotification] {
      let item = items[index - 1]
      if let font = UIFont(name: FontNames.Medium, size: 18) {
        titleHeight = item.title.renderHeight(contextWith: contentContainerWidth, font: font)
      }

      if let font = UIFont(name: FontNames.Light, size: 10) {
        subTitleHeight = item.title.renderHeight(contextWith: contentContainerWidth, font: font)
      }

      if let font = UIFont(name: FontNames.Regular, size: 12) {
        detailsHeight = item.details.renderHeight(contextWith: contentContainerWidth, font: font)
      }
    }

    /// This is to measure the height of the hight of the cell
    let baseHeightAndMargin = NotificationCollectionViewCell.containerMarginHorizontal / 1.4 + margin * 3
    return CGSize(width: contextWidth, height: titleHeight + subTitleHeight + detailsHeight + baseHeightAndMargin)
  }

  func itemCell(_ context: Context,
                selectedItem: AnyObject,
                index: Int,
                sectionController: ListSectionController
  ) -> UICollectionViewCell {
    let cell = context.dequeueReusableCell(of: NotificationCollectionViewCell.self,
                                           withReuseIdentifier: "NOTIFICATION",
                                           for: sectionController,
                                           at: index)

    guard let notificationCell = cell as? NotificationCollectionViewCell,
      let items = selectedItem as? [RemoteNotification] else {
      return cell
    }

    let item = items[index - 1]

    notificationCell.setupDataCell(with: item)
    print(item.notificationType)

    return notificationCell
  }
}
