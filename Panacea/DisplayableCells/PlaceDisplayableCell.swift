//
//  PlaceDisplayableCell.swift
//  Panacea
//
//  Created by Luis Romero on 10/9/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class PlaceDisplayableCell: SectionCellDisplayable {
  func sizeForItem(at index: Int, selectedItem: AnyObject, contextWidth: CGFloat) -> CGSize {
    let numberOfItemPerRow = UIDevice.current.userInterfaceIdiom == .pad ? 2 : 1
    let itemWidth = contextWidth / CGFloat(numberOfItemPerRow)
    var cellHeight = CGFloat(240)
    var titleHeight = CGFloat(30)

    if let items = selectedItem as? [DisplayableTile] {
      let item = items[index - 1]

      if let place = item as? Place, place.placeType == PlaceType.place {
        cellHeight -= CGFloat(20)
      }

      if let font = UIFont(name: FontNames.Medium, size: 18) {
        titleHeight = (item.title ?? "").renderHeight(contextWith: contextWidth, font: font)
      }
    }

    return CGSize(width: itemWidth, height: cellHeight + titleHeight)
  }

  func itemCell(_ context: Context,
                selectedItem: AnyObject,
                index: Int,
                sectionController: ListSectionController
  ) -> UICollectionViewCell {
    let cell = context.dequeueReusableCell(of: TourCollectionViewCell.self,
                                           withReuseIdentifier: "TOUR",
                                           for: sectionController,
                                           at: index)

    guard let placeCell = cell as? TourCollectionViewCell else {
      return cell
    }

    guard let items = selectedItem as? [DisplayableTile] else {
      return cell
    }

    guard let tour =  items[index - 1] as? Place else {
      return cell
    }

    guard let title = tour.placeTitle,
      let description = tour.placeDescription,
      let imageUrl = tour.placeImageHeaderUrl else {
      return cell
    }

    let price = tour.placePrice ?? 0

    let dateString = "\(tour.dueDateString())\n(\(tour.remainingDays()))"
    placeCell.setupDataCell(title: title,
                            description: description,
                            imageUrl: imageUrl,
                            price: Int(price),
                            date: dateString)

    if tour.placeType == PlaceType.place {
      placeCell.tourDate.isHidden = true
    }

    if tour.price?.isLessThanOrEqualTo(1.0) ?? false {
      placeCell.tourPrice.isHidden = true
    }

    return placeCell
  }
}
