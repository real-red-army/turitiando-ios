//
//  SkeletonDisplayableCell.swift
//  Panacea
//
//  Created by Luis Romero on 10/27/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

enum SkeletonDisplayType {
  case details
  case card
}

class SkeletonDisplayableCell: SectionCellDisplayable {
  private let skeletonType: SkeletonDisplayType

  init(with type: SkeletonDisplayType) {
    self.skeletonType = type
  }

  func sizeForItem(at index: Int, selectedItem: AnyObject, contextWidth: CGFloat) -> CGSize {
    if skeletonType == .card {
      return CGSize(width: contextWidth, height: 160)
    }

    return CGSize(width: contextWidth, height: 70)
  }

  func itemCell(_ context: Context,
                selectedItem: AnyObject,
                index: Int,
                sectionController: ListSectionController
  ) -> UICollectionViewCell {
    let rawCell = skeletonCell(context, index: index, sectionController: sectionController)

    if let skeletonCell = rawCell as? TitleDetailsSkeletonCell {
      skeletonCell.gradientLayers.forEach { (gradientLayer) in
        let baseColor = UIColor(hexValue: Colors.LightGray)
        let middleColor = UIColor(hexValue: Colors.LighterGray)

        gradientLayer.colors = [baseColor.cgColor, middleColor.cgColor, baseColor.cgColor]
      }

      skeletonCell.slide(to: .right)
    }

    return rawCell
  }

  private func skeletonCell(_ context: Context,
                            index: Int,
                            sectionController: ListSectionController) -> UICollectionViewCell {
    if self.skeletonType == .card {
      return context.dequeueReusableCell(of: CardSkeletonCollectionCell.self,
                                         withReuseIdentifier: "EMPTY",
                                         for: sectionController, at: index)
    }

    return context.dequeueReusableCell(of: TitleDetailsSkeletonCell.self,
                                       withReuseIdentifier: "EMPTY",
                                       for: sectionController, at: index)
  }
}
