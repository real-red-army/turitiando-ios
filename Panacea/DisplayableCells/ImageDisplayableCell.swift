//
//  ImageDisplayableCell.swift
//  Panacea
//
//  Created by Luis Romero on 11/4/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class ImageDisplayableCell: SectionCellDisplayable {
  func itemCell(_ context: Context,
                selectedItem: AnyObject,
                index: Int,
                sectionController: ListSectionController) -> UICollectionViewCell {
    let cell = context.dequeueReusableCell(of: TileImageViewCell.self,
                                           withReuseIdentifier: "TileImageViewCell",
                                           for: sectionController,
                                           at: index)

    guard let cellImage = cell as? TileImageViewCell, let item = selectedItem as? PlaceImage else {
      return UICollectionViewCell(frame: .zero)
    }

    cellImage.setupImage(item.imageUrl)

    return cellImage
  }

  func sizeForItem(at index: Int, selectedItem: AnyObject, contextWidth: CGFloat) -> CGSize {
    return CGSize(width: 90, height: 90)
  }
}
