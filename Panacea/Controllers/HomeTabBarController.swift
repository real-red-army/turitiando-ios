//
//  HomeTabBarController.swift
//  Panacea
//
//  Created by Blashadow on 6/15/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {
  let homeFeedCoordinator = HomeFeedCoordinator()
  let placeHomeList = HomePlaceListCoordinator()
  let tourHomeList = BaseTourListCoordinator()
  let notificationList = NotificationListCoordinator()

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  func setupTabBarController() {
    var controllers: [UIViewController] = []

    controllers.append(homeFeedCoordinator.homeFeedController)
    controllers.append(placeHomeList.homePlaceListController)
    controllers.append(tourHomeList.tourHomeListController)
    controllers.append(notificationList.notificationListController)

    self.viewControllers = controllers
  }
}
