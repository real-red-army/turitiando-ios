//
//  LightBoxViewController.swift
//  Panacea
//
//  Created by Luis Romero on 10/5/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke
import Lightbox

class LightBoxViewController: LightboxController {
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  static func presentLightBoxController(_ controller: UIViewController, images: [PlaceImage]) {
    LightboxConfig.loadImage = { imageView, imageURL, completion in
      let option = ImageLoadingOptions.shared
      Nuke.loadImage(with: imageURL, options: option, into: imageView, progress: nil) { (image, _) in
        if let completion = completion {
          completion(image?.image)
        }
      }
    }

    // Create an array of images.
    let items: [LightboxImage?] = images.map({ (image: PlaceImage) -> LightboxImage? in
      guard let urlImage = URL(string: image.imageUrl) else {
        return nil
      }

      return LightboxImage(imageURL: urlImage)
    }).filter {$0 != nil}

    guard let images = items as? [LightboxImage] else {
      return
    }

    // Create an instance of LightboxController.
    let lightBoxController = LightBoxViewController(images: images)
    lightBoxController.modalPresentationStyle = .fullScreen

    // Use dynamic background.
    lightBoxController.dynamicBackground = true

    // Present your controller.
    controller.present(lightBoxController, animated: true, completion: nil)
  }
}
