//
//  ViewController.swift
//  Panacea
//
//  Created by Blashadow on 10/20/18.
//  Copyright © 2018 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class ViewController: UIViewController {

  let updater = ListAdapterUpdater()
  lazy var adapter: ListAdapter = {
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  var items = [Place]()
  @IBOutlet weak var collectionView: UICollectionView!

  override func viewDidLoad() {
    super.viewDidLoad()

    adapter.collectionView = collectionView
    adapter.dataSource = self

    //Class places
    _ = WebServiceClient().retreivePlaces(with: { [unowned self](_, items) in
      let places = items?.map({ (rawItem) -> Place in
        let item = Place()

        item.identifier = rawItem["id"] as? Int ?? 0
        item.placeTitle = rawItem["title"] as? String
        item.placeImageHeaderUrl = rawItem["image_cover"] as? String
        item.favoriteCount = rawItem["favorites"] as? Int

        return item
      })

      self.items.append(contentsOf: places!)

      self.adapter.performUpdates(animated: true, completion: nil)
    })
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension ViewController: ListAdapterDataSource {
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    return items
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    return SectionController(nil)
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}
