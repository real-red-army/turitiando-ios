//
//  BaseListViewController.swift
//  Panacea
//
//  Created by Blashadow on 6/23/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

protocol DisplayableListViewController {
  func fetchContent()
}

class BaseListViewController: UIViewController {
  var displayableListController: DisplayableListViewController?
  var adapterDataSource: (ListDataSource & ListAdapterDataSource)? {
    didSet {
      adapter.dataSource = adapterDataSource
    }
  }

  lazy var adapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  let collectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    view.backgroundColor = UIColor.white
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var requestFetchSession: URLSessionDataTask?
  var animateCollectionView = AnimatedCollectionView()

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = UIColor.white
    view.addSubview(collectionView)

    NSLayoutConstraint.activate([
      collectionView.widthAnchor.constraint(equalTo: view.widthAnchor),
      collectionView.heightAnchor.constraint(equalTo: view.heightAnchor),
      collectionView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
    ])

    // Setup collection view
    adapter.collectionView = self.collectionView
    adapter.dataSource = self.adapterDataSource
    adapter.collectionViewDelegate = animateCollectionView
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    // Load tours
    displayableListController?.fetchContent()
  }
}
