//
//  PathDetailsView.swift
//  Panacea
//
//  Created by Luis Romero on 10/14/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class PathDetailsView: UIView {
  var scrollView: UIScrollView = {
    let view = UIScrollView(frame: .zero)
    view.backgroundColor = UIColor.white
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var closeButton = CircleCloseButton(frame: .zero)

  var headerImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true

    return view
  }()

  var imageContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var title: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 18)
    label.backgroundColor = UIColor.white
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  var details: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 14)
    label.textColor = UIColor(hexValue: Colors.Description)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  var placesContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var targetDestinationsLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 16)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Destinations"

    return label
  }()

  var placesList: UICollectionView = {
    let layout = UICollectionViewFlowLayout()

    let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layoutMargins = .zero
    view.backgroundColor = UIColor.white
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false

    return view
  }()

  init(frame: CGRect, numerOfPlaces: Int) {
    super.init(frame: frame)

    setupConstraints(numerOfPlaces)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupConstraints(_ numerOfPlaces: Int) {
    translatesAutoresizingMaskIntoConstraints = false
    addSubview(scrollView)

    NSLayoutConstraint.activate([
      scrollView.widthAnchor.constraint(equalTo: widthAnchor),
      scrollView.heightAnchor.constraint(equalTo: heightAnchor),
      scrollView.centerXAnchor.constraint(equalTo: centerXAnchor),
      scrollView.centerYAnchor.constraint(equalTo: centerYAnchor)
    ])

    scrollView.addSubview(imageContainer)
    imageContainer.addSubview(headerImage)

    setupImageContainer()

    setupCloseButtonConstraint()

    scrollView.addSubview(title)

    NSLayoutConstraint.activate([
      title.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0, constant: -10),
      title.heightAnchor.constraint(greaterThanOrEqualToConstant: 20),
      title.topAnchor.constraint(equalTo: imageContainer.bottomAnchor, constant: 10),
      title.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    scrollView.addSubview(details)
    NSLayoutConstraint.activate([
      details.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0, constant: -10),
      details.heightAnchor.constraint(greaterThanOrEqualToConstant: 50),
      details.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 10),
      details.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    setupPlacesContainerContent(with: numerOfPlaces)
  }

  // MARK: - Image Container
  private func setupImageContainer() {
    NSLayoutConstraint.activate([
      imageContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
      imageContainer.heightAnchor.constraint(equalToConstant: 250),
      imageContainer.topAnchor.constraint(equalTo: scrollView.topAnchor),
      imageContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    let constraint = headerImage.topAnchor.constraint(lessThanOrEqualTo: self.topAnchor)
    constraint.priority = UILayoutPriority.defaultHigh

    let heightConstraint = headerImage.heightAnchor.constraint(
      greaterThanOrEqualTo: imageContainer.heightAnchor
    )
    constraint.priority = UILayoutPriority.required

    NSLayoutConstraint.activate([
      headerImage.widthAnchor.constraint(equalTo: imageContainer.widthAnchor),
      constraint,
      heightConstraint,
      headerImage.bottomAnchor.constraint(equalTo: imageContainer.bottomAnchor),
      headerImage.centerXAnchor.constraint(equalTo: imageContainer.centerXAnchor)
    ])
  }

  private func setupPlacesContainerContent(with numberOfPlaces: Int) {
    scrollView.addSubview(placesContainer)

    if numberOfPlaces == 0 {
      placesContainer.isHidden = true

      NSLayoutConstraint.activate([
        placesContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        placesContainer.heightAnchor.constraint(equalToConstant: 0),
        placesContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
        placesContainer.topAnchor.constraint(equalTo: details.bottomAnchor, constant: 10)
      ])

      return
    }

    let totalHeight = numberOfPlaces == 1 ? 150 : numberOfPlaces * Int(TourTileController.tourTileHeight)
    placesContainer.clipsToBounds = true
    NSLayoutConstraint.activate([
      placesContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
      placesContainer.heightAnchor.constraint(greaterThanOrEqualToConstant: CGFloat(totalHeight)),
      placesContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
      placesContainer.topAnchor.constraint(equalTo: details.bottomAnchor, constant: 10),
      placesContainer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
    ])

    placesContainer.addSubview(targetDestinationsLabel)

    NSLayoutConstraint.activate([
      targetDestinationsLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
      targetDestinationsLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      targetDestinationsLabel.leftAnchor.constraint(equalTo: placesContainer.leftAnchor, constant: 5),
      targetDestinationsLabel.topAnchor.constraint(equalTo: placesContainer.topAnchor)
    ])

    placesContainer.addSubview(placesList)

    NSLayoutConstraint.activate([
      placesList.widthAnchor.constraint(equalTo: placesContainer.widthAnchor, multiplier: 1.0),
      placesList.heightAnchor.constraint(equalToConstant: CGFloat(totalHeight)),
      placesList.topAnchor.constraint(equalTo: targetDestinationsLabel.bottomAnchor, constant: 10),
      placesList.bottomAnchor.constraint(equalTo: placesContainer.bottomAnchor),
      placesList.centerXAnchor.constraint(equalTo: placesContainer.centerXAnchor)
    ])
  }

  fileprivate func setupCloseButtonConstraint() {
    scrollView.addSubview(closeButton)

    NSLayoutConstraint.activate([
      closeButton.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 15),
      closeButton.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 15)
    ])

    let constraint = closeButton.topAnchor.constraint(lessThanOrEqualTo: self.topAnchor, constant: 40)
    constraint.priority = UILayoutPriority.required
    constraint.isActive = true
  }
}
