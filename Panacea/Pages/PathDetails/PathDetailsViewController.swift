//
//  PathDetailsViewController.swift
//  Panacea
//
//  Created by Luis Romero on 10/14/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke
import IGListKit

class PathDetailsViewController: UIViewController {
  private let animateCollectionView = AnimatedCollectionView()
  lazy var destinationsAdapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  var pathDetailsView: PathDetailsView?
  var path: Path?

  override func viewDidLoad() {
    super.viewDidLoad()

    pathDetailsView = PathDetailsView(frame: .zero, numerOfPlaces: path?.places.count ?? 0)

    setupView()

    setupData()
  }

  private func setupData() {
    guard let path = path, let pathDetailsView = pathDetailsView else {
      return
    }

    pathDetailsView.title.text = path.title
    pathDetailsView.details.text = path.details

    if let urlImage = URL(string: path.imageUrl) {
      Nuke.loadImage(with: urlImage, into: pathDetailsView.headerImage)
    }

    setupAdapters()
  }

  private func setupView() {
    guard let pathDetailsView = pathDetailsView else {
      return
    }

    view.addSubview(pathDetailsView)

    NSLayoutConstraint.activate([
      pathDetailsView.widthAnchor.constraint(equalTo: view.widthAnchor),
      pathDetailsView.heightAnchor.constraint(equalTo: view.heightAnchor),
      pathDetailsView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      pathDetailsView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
    ])

    pathDetailsView.closeButton.tapHandler = closeViewController
  }

  private func setupAdapters() {
    guard let pathDetailsView = pathDetailsView else {
      return
    }

    destinationsAdapter.collectionView = pathDetailsView.placesList
    destinationsAdapter.dataSource = self
    destinationsAdapter.collectionViewDelegate = animateCollectionView
  }

  @objc func closeViewController() {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }

  private func placePressHandler(_ place: DisplayableTile) {
    guard let place = place as? Place else {
      return
    }

    _ = PlaceDataHandler
        .shared
        .retrievePlaceInfo(with: place.identifier) {[weak self] (_, place) in
      guard let place = place else {
        return
      }

      let placeDetailsController = PlaceDetailsViewController()
      placeDetailsController.place = place

      self?.navigationController?.pushViewController(placeDetailsController, animated: true)
    }
  }

}

extension PathDetailsViewController: ListAdapterDataSource {
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    return self.path?.places ?? []
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    return TourTileController(with: placePressHandler, style: .fullWidth)
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}
