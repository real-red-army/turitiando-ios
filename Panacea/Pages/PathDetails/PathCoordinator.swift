//
//  PathCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/1/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class PathCoordinator: Coordinator {
  var dismissCoordinator: (() -> Void)?
  
  let path: Path
  let navigator: UIViewController

  init(path: Path, navigator: UIViewController) {
    self.path = path
    self.navigator = navigator
  }

  func start() {
    let controller = PathDetailsViewController()
    controller.path = path

    let button = UIBarButtonItem(title: "Cancelar",
                                 style: .plain,
                                 target: controller,
                                 action: #selector(controller.closeViewController))

    let navigationController = UINavigationController(rootViewController: controller)
    navigationController.topViewController?.navigationItem.title = path.name
    navigationController.topViewController?.navigationItem.leftBarButtonItem = button
    navigationController.modalPresentationStyle = .fullScreen
    navigationController.isNavigationBarHidden = true

    self.navigator.present(navigationController, animated: true, completion: nil)
  }
}
