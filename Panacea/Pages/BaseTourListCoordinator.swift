//
//  HomeTourListCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/12/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class BaseTourListCoordinator: Coordinator, BaseToursListNavigationProtocol {
  var dismissCoordinator: (() -> Void)?
  
  let tourHomeListController: HomeToursViewController
  var tourDetailsCoordinator: TourDetailsCoordinator?

  init() {
    tourHomeListController = HomeToursViewController()
    tourHomeListController.tabBarItem = UITabBarItem(title: "Tours", image: UIImage(named: "TabBarTours"), tag: 0)
    tourHomeListController.navigationDelegate = self
  }

  func start() {
  }

  func dismiss() {
    if let handler = self.dismissCoordinator {
      handler()
    }
  }

  func tourDetails(tour: Place) {
    tourDetailsCoordinator = TourDetailsCoordinator(tour: tour, navigator: tourHomeListController)
    tourDetailsCoordinator?.start()
  }
}
