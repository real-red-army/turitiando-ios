//
//  HomePlaceListCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/12/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class HomePlaceListCoordinator: Coordinator, HomePlaceNavigationProtocol {
  var dismissCoordinator: (() -> Void)?
  
  var homePlaceListController: HomePlacesViewController
  var placeDetailsCoordinator: PlaceDetailsCoordinator?

  init() {
    homePlaceListController = HomePlacesViewController()
    homePlaceListController.tabBarItem = UITabBarItem(title: "Destinos", image: UIImage(named: "TabBarPlaces"), tag: 0)
    homePlaceListController.navigationDelegate = self
  }

  func start() {
  }

  func placeDetails(place: Place) {
    placeDetailsCoordinator = PlaceDetailsCoordinator(place: place, navigator: homePlaceListController)
    placeDetailsCoordinator?.start()
  }
}
