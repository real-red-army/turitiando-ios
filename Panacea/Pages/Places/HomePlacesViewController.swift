//
//  HomePlacesViewController.swift
//  Panacea
//
//  Created by Blashadow on 6/23/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

protocol HomePlaceNavigationProtocol: AnyObject {
  func placeDetails(place: Place)
}

class HomePlacesViewController: BaseListViewController {
  weak var navigationDelegate: HomePlaceNavigationProtocol?

  override func viewDidLoad() {
    super.viewDidLoad()

    displayableListController = self
    self.adapterDataSource = SectionDataSource(PlaceDisplayableCell(), items: [])
  }
}

extension HomePlacesViewController: DisplayableListViewController {
  func fetchContent() {
    self.requestFetchSession = PlaceDataHandler.shared.retrieveListOfPlaces { (_, places) in
      guard let places = places else {
        return
      }

      let firstSection = SectionItems(title: "Destinations", itemType: .place(places: places))
      firstSection.itemPressHandler = self.pressHandler

      let sectionsList: [SectionItems] = [firstSection]
      self.adapterDataSource?.setItems(sectionsList)

      self.adapter.performUpdates(animated: true, completion: nil)
    }
  }

  func pressHandler(_ selectedItem: Any) {
    guard let place = selectedItem as? Place,
      let navigationDelegate = navigationDelegate else {
      return
    }

    navigationDelegate.placeDetails(place: place)
  }
}
