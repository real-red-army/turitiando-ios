//
//  HomeFeedCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/1/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class HomeFeedCoordinator: Coordinator {
  var dismissCoordinator: (() -> Void)?
  
  let homeFeedController: HomeFeedViewController
  var placeDetailsCoordinator: PlaceDetailsCoordinator?
  var tourDetailsCoordinator: TourDetailsCoordinator?
  var pathDetailsCoordinator: PathCoordinator?

  init () {
    homeFeedController = HomeFeedViewController(nibName: nil, bundle: nil)
    homeFeedController.tabBarItem = UITabBarItem(title: "Inicio", image: UIImage(named: "TabBarHome"), tag: 0)
    homeFeedController.navigationDelegate = self
  }

  func start() {}

  private func dismissPlaceDetailsCoordinator() {
    self.placeDetailsCoordinator = nil
  }

  private func dismissPathDetailsCoordinator() {
    self.pathDetailsCoordinator = nil
  }

  private func dismissTourDetailsCoordinator() {
    self.tourDetailsCoordinator = nil
  }
}

extension HomeFeedCoordinator: HomeFeedNavigationProtocol {
  func placeDetails(place: Place) {
    self.placeDetailsCoordinator = PlaceDetailsCoordinator(place: place, navigator: homeFeedController)
    self.placeDetailsCoordinator?.start()
    self.placeDetailsCoordinator?.dismissCoordinator = dismissPlaceDetailsCoordinator
  }

  func pathDetails(path: Path) {
    self.pathDetailsCoordinator = PathCoordinator(path: path, navigator: homeFeedController)
    self.pathDetailsCoordinator?.start()
    self.pathDetailsCoordinator?.dismissCoordinator = dismissPathDetailsCoordinator
  }

  func tourDetails(tour: Place) {
    self.tourDetailsCoordinator = TourDetailsCoordinator(tour: tour, navigator: homeFeedController)
    self.tourDetailsCoordinator?.start()
    self.tourDetailsCoordinator?.dismissCoordinator = dismissTourDetailsCoordinator
  }
}
