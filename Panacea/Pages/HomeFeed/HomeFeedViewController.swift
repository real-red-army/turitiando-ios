//
//  HomeFeedViewController.swift
//  Panacea
//
//  Created by Blashadow on 6/15/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

protocol HomeFeedNavigationProtocol {
  func placeDetails(place: Place)
  func pathDetails(path: Path)
  func tourDetails(tour: Place)
}

class HomeFeedViewController: UIViewController {
  // swiftlint:disable weak_delegate
  var navigationDelegate: HomeFeedNavigationProtocol?

  lazy var adapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  let collectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    let view = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
    view.backgroundColor = UIColor.white
    view.layoutMargins = .zero

    return view
  }()

  private var homeFeedViewModel: HomeFeedViewModel?

  override func viewDidLoad() {
    super.viewDidLoad()

    // Setup collection view
    self.view.addSubview(self.collectionView)
    adapter.collectionView = self.collectionView
    adapter.dataSource = self

    self.homeFeedViewModel = HomeFeedViewModel()
    self.homeFeedViewModel?.tourItemSelectionHandler = self.tourItemSelectionHandler
    self.homeFeedViewModel?.openTourTab = self.openTourTab

    self.homeFeedViewModel?.placeItemSelectionHandler = self.placeItemSelectionHandler
    self.homeFeedViewModel?.openPlaceTab = self.openPlaceTab

    self.homeFeedViewModel?.pathItemSelectionHandler = self.pathItemSelectionHandler
    self.homeFeedViewModel?.openPathsTab = self.openPlaceTab

    self.homeFeedViewModel?.completionHandler = self.updateList

    self.homeFeedViewModel?.fetchInfo()
  }

  private func openPlaceTab() {
    self.tabBarController?.selectedIndex = 2
  }

  private func placeItemSelectionHandler(with place: Any) {
    guard let place = place as? Place else {
      return
    }

    self.navigationDelegate?.placeDetails(place: place)
  }

  private func openTourTab() {
    self.tabBarController?.selectedIndex = 2
  }

  private func tourItemSelectionHandler(with place: Any) {
    guard let tour = place as? Place else {
      return
    }

    self.navigationDelegate?.tourDetails(tour: tour)
  }

  private func pathItemSelectionHandler(with path: Any) {
    guard let path = path as? Path else {
      return
    }

    self.navigationDelegate?.pathDetails(path: path)
  }

  private func updateList() {
    self.adapter.performUpdates(animated: true, completion: nil)
  }
}

extension HomeFeedViewController: ListAdapterDataSource {
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    return self.homeFeedViewModel?.items ?? []
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    return HorizontalSectionController(nil)
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}
