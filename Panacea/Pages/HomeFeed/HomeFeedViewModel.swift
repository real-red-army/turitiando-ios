//
//  HomeFeedViewModel.swift
//  Panacea
//
//  Created by Blashadow on 6/15/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class HomeFeedViewModel: NSObject {
  private var placeRequestSession: URLSessionDataTask?
  private var toursRequestSession: URLSessionDataTask?
  private var pathsRequestSession: URLSessionDataTask?

  var items = [SectionItems]()

  var placeaItemsSection: SectionItems?
  var toursItemsSection: SectionItems?
  var pathsItemsSection: SectionItems?

  var openPlaceTab: (() -> Void)?
  var placeItemSelectionHandler: ((_: Any) -> Void)?

  var openTourTab: (() -> Void)?
  var tourItemSelectionHandler: ((_: Any) -> Void)?

  var openPathsTab: (() -> Void)?
  var pathItemSelectionHandler: ((_: Any) -> Void)?

  var completionHandler: (() -> Void)?

  func fetchInfo() {
    // Fetch places
    fetchPlaces()

    // Fetch tours
    fetchTours()

    // Fetch paths
    fetchPaths()
  }

  private func fetchPlaces() {
    // Class places
    self.placeRequestSession = PlaceDataHandler.shared.retrieveListOfPlaces { (_, places) in
      if let places = places {
        self.placeaItemsSection = SectionItems(title: "Destinos destacados", itemType: .placeTile(places: places))
        self.placeaItemsSection?.headerPressHandler = self.openPlaceTab
        self.placeaItemsSection?.itemPressHandler = self.placeItemSelectionHandler
      }

      self.updateList()
    }
  }

  private func fetchTours() {
    // Class places
    self.toursRequestSession = PlaceDataHandler.shared.retrieveListOfTours { (_, places) in
      if let places = places {
        self.toursItemsSection = SectionItems(title: "Mejores Ofertas", itemType: .tourTile(tours: places))
        self.toursItemsSection?.headerPressHandler = self.openTourTab
        self.toursItemsSection?.itemPressHandler = self.tourItemSelectionHandler
      }

      self.updateList()
    }
  }

  func fetchPaths() {
    // Class PAths
    self.pathsRequestSession = PathRequestDataHandler.shared.retrieveListOfPaths(with: { (_, paths) in
      if let paths = paths {
        self.pathsItemsSection = SectionItems(title: "Rutas", itemType: .paths(paths: paths))
        self.pathsItemsSection?.headerPressHandler = self.openPathsTab
        self.pathsItemsSection?.itemPressHandler = self.pathItemSelectionHandler
      }

      self.updateList()
    })
  }

  private func updateList() {
    self.items.removeAll()

    if let firstSection = self.placeaItemsSection {
      self.items.append(firstSection)
    }

    if let secondSection = self.toursItemsSection {
      self.items.append(secondSection)
    }

    if let thirdSection = self.pathsItemsSection {
      self.items.append(thirdSection)
    }

    if let updateHandler = self.completionHandler {
      updateHandler()
    }
  }
}
