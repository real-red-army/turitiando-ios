//
//  NotificationListCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/15/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class NotificationListCoordinator: Coordinator {
  var dismissCoordinator: (() -> Void)?
  
  var notificationListController: NotificationsListViewController
  var placeDetailsCoordinator: PlaceDetailsCoordinator?
  var tourDetailsCoordinator: TourDetailsCoordinator?

  init() {
    notificationListController = NotificationsListViewController()
    notificationListController.navigationDelegate = self

    let item = UITabBarItem(title: "Notificaciones", image: UIImage(named: "TabBarNotifications"), tag: 0)
    notificationListController.tabBarItem = item
  }

  func start() {
  }

  private func dismissTourDetailsCoordinator() {
    self.tourDetailsCoordinator = nil
  }

  private func dismissPlaceDetailsCoordinator() {
    self.placeDetailsCoordinator = nil
  }
}

extension NotificationListCoordinator: NotificationListNavigationDelegate {
  func openTour(tour: Place) {
    tourDetailsCoordinator = TourDetailsCoordinator(tour: tour, navigator: notificationListController)
    tourDetailsCoordinator?.start()
    tourDetailsCoordinator?.dismissCoordinator = self.dismissTourDetailsCoordinator
  }

  func openPlace(place: Place) {
    placeDetailsCoordinator = PlaceDetailsCoordinator(place: place, navigator: notificationListController)
    placeDetailsCoordinator?.start()
    placeDetailsCoordinator?.dismissCoordinator = self.dismissPlaceDetailsCoordinator
  }
}
