//
//  NotificationsListViewController.swift
//  Panacea
//
//  Created by Luis Romero on 10/9/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

protocol NotificationListNavigationDelegate: AnyObject {
  func openTour(tour: Place)
  func openPlace(place: Place)
}

class NotificationsListViewController: BaseListViewController, DisplayableListViewController {
  weak var navigationDelegate: NotificationListNavigationDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()

    displayableListController = self
    let loadingSection = [SectionItems(title: "Notifications", itemType: .loading(dummyItems: [1, 2, 3, 4, 5]))]
    self.adapterDataSource = SectionDataSource(SkeletonDisplayableCell(with: .details), items: loadingSection)
  }

  func fetchContent() {
    let deadlineTime = DispatchTime.now() + .seconds(1)

    DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
      self?.loadNotifications()
    }
  }

  func pressHandler(_ selectedItem: Any) {
    guard let remoteNotification = selectedItem as? RemoteNotification else {
      return
    }

    if remoteNotification.type == .place {
      openPlace(Int(remoteNotification.entityId))
    } else if remoteNotification.type == .tour {
      openTour(Int(remoteNotification.entityId))
    }
  }

  private func openPlace(_ identifier: Int) {
    guard let navigationDelegate = self.navigationDelegate else {
      return
    }

    _ = PlaceDataHandler
      .shared
      .retrievePlaceInfo(with: identifier) { [navigationDelegate] (_, place) in

      guard let place = place else {
        return
      }

      navigationDelegate.openPlace(place: place)
    }
  }

  private func openTour(_ identifier: Int) {
    guard let navigationDelegate = self.navigationDelegate else {
      return
    }

    _ = PlaceDataHandler
      .shared
      .retrievePlaceInfo(with: identifier) { [navigationDelegate] (_, tour) in
      guard let tour = tour else {
        return
      }

      navigationDelegate.openTour(tour: tour)
    }
  }

  private func loadNotifications() {
    let dataAccess = DataAccess<RemoteNotification>()
    let results = dataAccess.listOfItems()

    let firstSection = SectionItems(title: "Notifications", itemType: .notification(notifications: Array(results)))
    firstSection.itemPressHandler = self.pressHandler

    let sectionsList: [SectionItems] = results.isEmpty ? [] : [firstSection]
    self.adapterDataSource = SectionDataSource(NotificationDisplayableCell(), items: sectionsList)

    self.adapter.reloadData(completion: nil)
  }
}
