//
//  TourListCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/30/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class TourListCoordinator: BaseTourListCoordinator {
  private let tours: [Place]
  private let navigator: UIViewController

  init(tours: [Place], navigator: UIViewController) {
    self.tours = tours
    self.navigator = navigator
  }

  override func start() {
    let controller = TourListViewController()
    controller.navigationDelegate = self
    controller.tours = tours
    controller.modalPresentationStyle = .fullScreen

    self.navigator.present(controller, animated: true, completion: nil)
  }
}
