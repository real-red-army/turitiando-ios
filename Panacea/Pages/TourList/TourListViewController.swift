//
//  TourListViewController.swift
//  Panacea
//
//  Created by Luis Romero on 11/30/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

protocol TourListNavigationDelegate: NavigationDelegate {}

class TourListViewController: BaseListToursViewController {
  var tours: [Place]?

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func contentList() {
    self.processItems(tours: self.tours ?? [])
  }

  override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
     super.dismiss(animated: flag, completion: completion)

     guard let navigationDelegate = navigationDelegate else {
       return
     }

     navigationDelegate.dismiss()
   }
}
