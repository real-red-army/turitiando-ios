//
//  MapDetailsViewController.swift
//  Panacea
//
//  Created by Luis Romero on 10/5/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import GoogleMaps

class MapDetailsViewController: UIViewController {
  var place: Place?
  var closeButton = CircleCloseButton(frame: .zero)

  override func viewDidLoad() {
    super.viewDidLoad()

    let latitude = self.place?.placeLocation?.latitude ?? 0
    let longitude = self.place?.placeLocation?.longitude ?? 0

    /// Create a GMSCameraPosition that tells the map to display the
    let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 10.0)
    let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
    mapView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(mapView)

    NSLayoutConstraint.activate([
      mapView.heightAnchor.constraint(equalTo: view.heightAnchor),
      mapView.widthAnchor.constraint(equalTo: view.widthAnchor),
      mapView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      mapView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
    ])

    /// Creates a marker in the center of the map.
    let marker = GMSMarker()

    marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    marker.title = place?.placeTitle
    marker.snippet = place?.placeDescription
    marker.icon = UIImage(named: "MapPin")
    marker.map = mapView
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    closeButton.tapHandler = closeViewController
    view.addSubview(closeButton)

    NSLayoutConstraint.activate([
      closeButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 45),
      closeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 15)
    ])
    view.bringSubview(toFront: closeButton)
  }

  @objc private func closeViewController() {
    navigationController?.popViewController(animated: true)
  }
}
