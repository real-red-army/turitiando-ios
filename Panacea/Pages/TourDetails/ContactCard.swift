//
//  ContactCard.swift
//  Panacea
//
//  Created by Blashadow on 7/22/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class ContactCard: UIView {
  lazy var contactTitle: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 14)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Contact label"

    return label
  }()

  lazy var sourceLink: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 14)
    label.textColor = UIColor(hexValue: Colors.LinkColor)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.isUserInteractionEnabled = true
    label.text = "Press link"

    return label
  }()

  private let topBorderView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = UIColor.lightGray

    return view
  }()

  private let bottomBorderView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = UIColor.lightGray

    return view
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupView()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupView() {
    translatesAutoresizingMaskIntoConstraints = false

//    layer.backgroundColor = UIColor(hexValue: 0xFaFaFa).cgColor
//    layer.shadowColor = UIColor.black.cgColor
//    layer.shadowOpacity = 0.2
//    layer.shadowRadius = 12
//    layer.shadowOffset = CGSize(width: 0, height: 0)
//    layer.cornerRadius = 5.0

    addSubview(topBorderView)
    addSubview(bottomBorderView)
    addSubview(contactTitle)
    addSubview(sourceLink)

    topBorderView.isHidden = true
    bottomBorderView.isHidden = true

    NSLayoutConstraint.activate([
      topBorderView.widthAnchor.constraint(equalTo: widthAnchor),
      topBorderView.heightAnchor.constraint(equalToConstant: 1),
      topBorderView.centerXAnchor.constraint(equalTo: centerXAnchor),
      topBorderView.topAnchor.constraint(equalTo: topAnchor)
    ])

    NSLayoutConstraint.activate([
      contactTitle.widthAnchor.constraint(equalTo: widthAnchor, constant: -10),
      contactTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      contactTitle.centerXAnchor.constraint(equalTo: centerXAnchor),
      contactTitle.topAnchor.constraint(equalTo: topAnchor, constant: 10)
    ])

    NSLayoutConstraint.activate([
      sourceLink.widthAnchor.constraint(equalTo: widthAnchor, constant: -10),
      sourceLink.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      sourceLink.centerXAnchor.constraint(equalTo: centerXAnchor),
      sourceLink.topAnchor.constraint(equalTo: contactTitle.bottomAnchor),
      sourceLink.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
    ])

    NSLayoutConstraint.activate([
      bottomBorderView.widthAnchor.constraint(equalTo: widthAnchor),
      bottomBorderView.heightAnchor.constraint(equalToConstant: 1),
      bottomBorderView.centerXAnchor.constraint(equalTo: centerXAnchor),
      bottomBorderView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
  }
}
