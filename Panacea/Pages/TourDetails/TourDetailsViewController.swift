//
//  TourDetailsViewController.swift
//  Panacea
//
//  Created by Blashadow on 6/23/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

protocol TourDetailsNavigationDelegate: NavigationDelegate {
}

class TourDetailsViewController: UIViewController {
  private var targetDetailsTask: URLSessionDataTask?
  private let animateCollectionView = AnimatedCollectionView()
  private lazy var tourDetailsView = TourDetailsView(frame: .zero, numberOfPlaces: tour?.targets?.count ?? 0)

  weak var navigationDelegate: TourDetailsNavigationDelegate?

  var tour: Place?
  var tourDetailsViewModel: TourDetailsViewModel?

  lazy var imageListAdapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  lazy var destinationsAdapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  override func viewDidLoad() {
    super.viewDidLoad()

    // Setup view constraint
    setupView()

    // Setup data view
    setupData()
  }

  private func setupView() {
    view.addSubview(tourDetailsView)

    NSLayoutConstraint.activate([
      tourDetailsView.widthAnchor.constraint(equalTo: view.widthAnchor),
      tourDetailsView.heightAnchor.constraint(equalTo: view.heightAnchor),
      tourDetailsView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      tourDetailsView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
    ])

    self.tourDetailsView.closeButton.tapHandler = closeViewController
    self.tourDetailsView.shareActionHandler = shareTour

    setupAdapters()
  }

  private func setupAdapters() {
    imageListAdapter.collectionView = tourDetailsView.imageList
    imageListAdapter.dataSource = self
    imageListAdapter.collectionViewDelegate = animateCollectionView

    destinationsAdapter.collectionView = tourDetailsView.placesList
    destinationsAdapter.dataSource = self
    destinationsAdapter.collectionViewDelegate = animateCollectionView
  }

  private func setupData() {
    guard let tour = self.tour else {
      return
    }

    // Create view model instance
    tourDetailsViewModel = TourDetailsViewModel(tour, view: tourDetailsView)
    tourDetailsViewModel?.setupDataView()
  }

  private func imagePressHandler(_ placeImage: PlaceImage) {
    LightBoxViewController.presentLightBoxController(self, images: tour?.images ?? [])
  }

  override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
    super.dismiss(animated: flag, completion: completion)

    guard let navigationDelegate = self.navigationDelegate else {
      return
    }

    navigationDelegate.dismiss()
  }

  private func placePressHandler(_ place: DisplayableTile) {
    guard let target = place as? PlaceTarget else {
      return
    }

    self.targetDetailsTask = PlaceDataHandler
      .shared
      .retrievePlaceInfo(with: target.identifier) {[weak self] (_, place) in
      guard let place = place else {
        return
      }

      let placeDetailsController = PlaceDetailsViewController()
      placeDetailsController.title = place.title
      placeDetailsController.place = place

      self?.navigationController?.pushViewController(placeDetailsController, animated: true)
    }
  }

  @objc func closeViewController() {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    if let task = self.targetDetailsTask, task.state == .running {
      task.cancel()
    }
  }

  private func shareTour() {
    // text to share
    guard let shareMessage = self.tour?.shareMessage else {
      return
    }

    share(content: shareMessage)
  }
}

extension TourDetailsViewController: ListAdapterDataSource {
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {

    if (imageListAdapter == listAdapter) {
      return self.tour?.images ?? []
    } else if destinationsAdapter == listAdapter {
      if let targets = self.tour?.targets {
        if targets.count > 1 {
          return Array(targets.prefix(upTo: 2))
        } else {
          return targets
        }
      }
    }

    return []
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    if self.destinationsAdapter == listAdapter {
      return TourTileController(with: placePressHandler, style: .fullWidth)
    }

    return GenericListSectionController(pressHandler: imagePressHandler, displayableItem: ImageDisplayableCell())
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}

extension TourDetailsViewController: ShareableController {}
