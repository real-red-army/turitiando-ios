//
//  TourDetailsViewModel.swift
//  Panacea
//
//  Created by Blashadow on 7/20/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke

class TourDetailsViewModel {
  private unowned let tour: Place
  private unowned let tourDetailsView: TourDetailsView

  init(_ tour: Place, view: TourDetailsView) {
    self.tour = tour
    self.tourDetailsView = view
  }

  func setupDataView() {
    let dataFormatter = DateFormatter()
    dataFormatter.locale = Locale.current
    dataFormatter.dateFormat = "E d MMM yyyy"

    tourDetailsView.tourTitle.text = tour.placeTitle?.capitalized
    tourDetailsView.tourDetails.text = tour.placeDescription?.capitalized
    tourDetailsView.tourPrice.text = "$\(Int(tour.placePrice ?? 0))"
    tourDetailsView.tourDate.text = dataFormatter.string(from: tour.dueDate ?? Date())

    tourDetailsView.contactCard.contactTitle.text = self.tour.companyName
    tourDetailsView.contactCard.sourceLink.text = self.tour.eventSource

    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openLink))
    tourDetailsView.contactCard.sourceLink.addGestureRecognizer(tapGesture)

    let tapGestureBooking = UITapGestureRecognizer(target: self, action: #selector(booking))
    tourDetailsView.bookingButton.addGestureRecognizer(tapGestureBooking)

    if let tourHeaderImageUrl = URL(string: tour.placeImageHeaderUrl ?? "") {
      Nuke.loadImage(with: tourHeaderImageUrl, into: tourDetailsView.tourHeaderImage)
    }
  }

  @objc private func booking() {
    URLHelper.openWhatApp(number: self.tour.companyPhone, source: self.tour.eventSource)
  }

  @objc private func openLink() {
    URLHelper.openUrl(urlString: self.tour.eventSource)
  }
}
