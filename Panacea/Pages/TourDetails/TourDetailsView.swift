//
//  TourDetailsView.swift
//  Panacea
//
//  Created by Blashadow on 7/19/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class TourDetailsView: UIView {
  let viewContentPadding: CGFloat = 10

  var shareActionHandler: (() -> Void)?

  var scrollView: UIScrollView = {
    let view = UIScrollView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var closeButton = CircleCloseButton(frame: .zero)

  var tourHeaderImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true

    return view
  }()

  var shareImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true
    view.isUserInteractionEnabled = true
    view.image = UIImage(named: "ShareIcon")

    return view
  }()

  var tourImageContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var tourTitle: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 18)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.clipsToBounds = true
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  var tourDate: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 14)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.text = "Sat 24 Jul 2019"
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  var tourPrice: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 18)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .right
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.text = "$2000"
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  var tourDetails: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 14)
    label.textColor = UIColor(hexValue: Colors.Description)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.isOpaque = true
    label.clipsToBounds = true
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  var imageList: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal

    let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layoutMargins = .zero
    view.backgroundColor = UIColor.white
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false

    return view
  }()

  var placesList: UICollectionView = {
    let layout = UICollectionViewFlowLayout()

    let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layoutMargins = .zero
    view.backgroundColor = UIColor.white
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false

    return view
  }()

  var placesContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var targetDestinationsLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 16)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Destinos"

    return label
  }()

  var viewAllDestinationsLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 12)
    label.textColor = UIColor(hexValue: Colors.SubTitle)
    label.textAlignment = .right
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Ver todos"

    return label
  }()

  var contactContainer: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var contactLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 16)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Contacto"

    return label
  }()

  var bookingButton: TextButton = TextButton(frame: .zero)
  var contactCard: ContactCard = ContactCard(frame: .zero)

  init(frame: CGRect, numberOfPlaces: Int) {
    super.init(frame: frame)

    setupView(with: numberOfPlaces)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Labels
  fileprivate func setupLabelsConstraints() {
    scrollView.addSubview(tourTitle)
    scrollView.addSubview(shareImage)

    NSLayoutConstraint.activate([
      shareImage.widthAnchor.constraint(equalToConstant: 30),
      shareImage.heightAnchor.constraint(equalToConstant: 30),
      shareImage.centerYAnchor.constraint(equalTo: tourTitle.centerYAnchor),
      shareImage.rightAnchor.constraint(equalTo: tourImageContainer.rightAnchor, constant: -10)
    ])

    NSLayoutConstraint.activate([
      tourTitle.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 10),
      tourTitle.rightAnchor.constraint(equalTo: shareImage.leftAnchor, constant: 10),
      tourTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: 20),
      tourTitle.topAnchor.constraint(equalTo: tourImageContainer.bottomAnchor, constant: 10)
    ])

    scrollView.addSubview(tourDate)
    NSLayoutConstraint.activate([
      tourDate.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.5, constant: -viewContentPadding),
      tourDate.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      tourDate.topAnchor.constraint(equalTo: tourTitle.bottomAnchor, constant: 10),
      tourDate.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 10)
    ])

    scrollView.addSubview(tourPrice)
    NSLayoutConstraint.activate([
      tourPrice.leftAnchor.constraint(equalTo: tourDate.rightAnchor),
      tourPrice.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.5, constant: -viewContentPadding),
      tourPrice.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      tourPrice.centerYAnchor.constraint(equalTo: tourDate.centerYAnchor)
    ])

    shareImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shareImageTapHandler)))
  }

  fileprivate func setupCloseButtonConstraint() {
    scrollView.addSubview(closeButton)

    NSLayoutConstraint.activate([
      closeButton.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 15),
      closeButton.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 15)
    ])

    let constraint = closeButton.topAnchor.constraint(lessThanOrEqualTo: self.topAnchor, constant: 40)
    constraint.priority = UILayoutPriority.required
    constraint.isActive = true
  }

  private func setupView(with numberOfPlaces: Int) {
    backgroundColor = UIColor.white
    translatesAutoresizingMaskIntoConstraints = false
    addSubview(scrollView)

    NSLayoutConstraint.activate([
      scrollView.widthAnchor.constraint(equalTo: widthAnchor),
      scrollView.heightAnchor.constraint(equalTo: heightAnchor),
      scrollView.centerXAnchor.constraint(equalTo: centerXAnchor),
      scrollView.centerYAnchor.constraint(equalTo: centerYAnchor)
    ])

    scrollView.addSubview(tourImageContainer)
    tourImageContainer.addSubview(tourHeaderImage)
    setupImageContainer()

    setupCloseButtonConstraint()

    // Setup label constraint
    setupLabelsConstraints()

    scrollView.addSubview(tourDetails)
    let padding = -2 * viewContentPadding

    NSLayoutConstraint.activate([
      tourDetails.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: padding),
      tourDetails.heightAnchor.constraint(greaterThanOrEqualToConstant: 50),
      tourDetails.topAnchor.constraint(equalTo: tourDate.bottomAnchor, constant: 10),
      tourDetails.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    scrollView.addSubview(imageList)
    NSLayoutConstraint.activate([
      imageList.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: padding),
      imageList.heightAnchor.constraint(greaterThanOrEqualToConstant: 100),
      imageList.topAnchor.constraint(equalTo: tourDetails.bottomAnchor, constant: 10),
      imageList.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    // Setup list of destinations
    scrollView.addSubview(placesContainer)
    setupPlacesContainerContent(with: numberOfPlaces)

    scrollView.addSubview(contactContainer)
    setupContactContainer()

    scrollView.addSubview(bookingButton)
    NSLayoutConstraint.activate([
      bookingButton.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.65),
      bookingButton.heightAnchor.constraint(equalToConstant: 45),
      bookingButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
      bookingButton.topAnchor.constraint(equalTo: contactContainer.bottomAnchor, constant: 10),
      bookingButton.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20)
    ])
  }

  // MARK: - Contact container
  private func setupContactContainer() {
    let padding = -2 * viewContentPadding

    NSLayoutConstraint.activate([
      contactContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: padding),
      contactContainer.heightAnchor.constraint(greaterThanOrEqualToConstant: 30),
      contactContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
      contactContainer.topAnchor.constraint(equalTo: placesContainer.bottomAnchor, constant: 10)
    ])

    contactContainer.addSubview(contactLabel)

    NSLayoutConstraint.activate([
      contactLabel.widthAnchor.constraint(equalTo: contactContainer.widthAnchor, constant: -10),
      contactLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      contactLabel.topAnchor.constraint(equalTo: contactContainer.topAnchor),
      contactLabel.centerXAnchor.constraint(equalTo: contactContainer.centerXAnchor)
    ])

    contactContainer.addSubview(contactCard)

    NSLayoutConstraint.activate([
      contactCard.widthAnchor.constraint(equalTo: contactContainer.widthAnchor, constant: -20),
      contactCard.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      contactCard.topAnchor.constraint(equalTo: contactLabel.bottomAnchor, constant: 10),
      contactCard.centerXAnchor.constraint(equalTo: contactContainer.centerXAnchor),
      contactCard.bottomAnchor.constraint(equalTo: contactContainer.bottomAnchor, constant: -20)
    ])
  }

  // MARK: - Image Container
  private func setupImageContainer() {
    NSLayoutConstraint.activate([
      tourImageContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
      tourImageContainer.heightAnchor.constraint(equalToConstant: 250),
      tourImageContainer.topAnchor.constraint(equalTo: scrollView.topAnchor),
      tourImageContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    let constraint = tourHeaderImage.topAnchor.constraint(lessThanOrEqualTo: self.topAnchor)
    constraint.priority = UILayoutPriority.defaultHigh

    let heightConstraint = tourHeaderImage.heightAnchor.constraint(
      greaterThanOrEqualTo: tourImageContainer.heightAnchor
    )
    constraint.priority = UILayoutPriority.required

    NSLayoutConstraint.activate([
      tourHeaderImage.widthAnchor.constraint(equalTo: tourImageContainer.widthAnchor),
      constraint,
      heightConstraint,
      tourHeaderImage.bottomAnchor.constraint(equalTo: tourImageContainer.bottomAnchor),
      tourHeaderImage.centerXAnchor.constraint(equalTo: tourImageContainer.centerXAnchor)
    ])
  }

  @objc private func shareImageTapHandler() {
    if let handler = self.shareActionHandler {
      handler()
    }
  }

  private func setupPlacesContainerContent(with numberOfPlaces: Int) {
    if numberOfPlaces == 0 {
      placesContainer.isHidden = true

      addSubview(placesContainer)

      NSLayoutConstraint.activate([
        placesContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -20),
        placesContainer.heightAnchor.constraint(equalToConstant: 0),
        placesContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
        placesContainer.topAnchor.constraint(equalTo: imageList.bottomAnchor, constant: 10)
      ])

      return
    }

    let totalHeight = numberOfPlaces == 1 ? 150 : 300
    placesContainer.clipsToBounds = true
    NSLayoutConstraint.activate([
      placesContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -20),
      placesContainer.heightAnchor.constraint(greaterThanOrEqualToConstant: CGFloat(totalHeight)),
      placesContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
      placesContainer.topAnchor.constraint(equalTo: imageList.bottomAnchor, constant: 10)
    ])

    placesContainer.addSubview(targetDestinationsLabel)

    NSLayoutConstraint.activate([
      targetDestinationsLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
      targetDestinationsLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      targetDestinationsLabel.leftAnchor.constraint(equalTo: placesContainer.leftAnchor, constant: 5),
      targetDestinationsLabel.topAnchor.constraint(equalTo: placesContainer.topAnchor)
    ])

    placesContainer.addSubview(viewAllDestinationsLabel)

    NSLayoutConstraint.activate([
      viewAllDestinationsLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
      viewAllDestinationsLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      viewAllDestinationsLabel.rightAnchor.constraint(equalTo: placesContainer.rightAnchor, constant: -5),
      viewAllDestinationsLabel.centerYAnchor.constraint(equalTo: targetDestinationsLabel.centerYAnchor)
    ])

    placesContainer.addSubview(placesList)

    NSLayoutConstraint.activate([
      placesList.widthAnchor.constraint(equalTo: placesContainer.widthAnchor, multiplier: 1.0),
      placesList.heightAnchor.constraint(equalToConstant: CGFloat(totalHeight)),
      placesList.topAnchor.constraint(equalTo: targetDestinationsLabel.bottomAnchor, constant: 10),
      placesList.bottomAnchor.constraint(equalTo: placesContainer.bottomAnchor),
      placesList.centerXAnchor.constraint(equalTo: placesContainer.centerXAnchor)
    ])
  }
}
