//
//  TourDetailsCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/1/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class TourDetailsCoordinator: Coordinator {
  var dismissCoordinator: (() -> Void)?

  let tour: Place
  let navigator: UIViewController

  init(tour: Place, navigator: UIViewController) {
    self.tour = tour
    self.navigator = navigator
  }

  func start() {
    let controller = TourDetailsViewController()
    controller.tour = tour
    controller.navigationDelegate = self

    let button = UIBarButtonItem(title: "Cancelar",
                                 style: .plain,
                                 target: controller,
                                 action: #selector(controller.closeViewController))

    let navigationController = UINavigationController(rootViewController: controller)
    navigationController.topViewController?.navigationItem.title = "Tour details"
    navigationController.topViewController?.navigationItem.leftBarButtonItem = button
    navigationController.modalPresentationStyle = .fullScreen
    navigationController.isNavigationBarHidden = true

    self.navigator.present(navigationController, animated: true, completion: nil)
  }
}

extension TourDetailsCoordinator: TourDetailsNavigationDelegate {
  func dismiss() {
    if let handler = self.dismissCoordinator {
      handler()
    }
  }
}
