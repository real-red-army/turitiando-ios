//
//  BaseListToursViewController.swift
//  Panacea
//
//  Created by Luis Romero on 11/30/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

protocol BaseToursListNavigationProtocol: NavigationDelegate {
  func tourDetails(tour: Place)
}

class BaseListToursViewController: BaseListViewController {
  weak var navigationDelegate: BaseToursListNavigationProtocol?

  override func viewDidLoad() {
    super.viewDidLoad()

    self.displayableListController = self
    let loadingSection = [SectionItems(title: "Tours", itemType: .loading(dummyItems: [1, 2, 3, 4]))]
    self.adapterDataSource = SectionDataSource(SkeletonDisplayableCell(with: .card), items: loadingSection)
  }

  func processItems(tours: [Place]) {
    let firstSection = SectionItems(title: "Tours", itemType: .tour(tours: tours))
    firstSection.itemPressHandler = self.pressHandler

    let sectionsList: [SectionItems] = self.sectioItems(with: tours)
    self.adapterDataSource = SectionDataSource(PlaceDisplayableCell(), items: sectionsList)

    self.adapter.reloadData(completion: nil)
  }

  func contentList() {}
}

extension BaseListToursViewController: DisplayableListViewController {
  func fetchContent() {
    contentList()
  }

  private func sectioItems(with tours: [Place]) -> [SectionItems] {
    var holdersItems: [String: [Place]] = [:]

    for tour in tours {
      if let date = tour.dueDate {
        let keyDictionary = String(date.month)
        if holdersItems[keyDictionary] != nil {
          holdersItems[keyDictionary]?.append(tour)
        } else {
          holdersItems[keyDictionary] = [tour]
        }
      }
    }

    let months: [Int: String] = [11: "Noviembre", 12: "Diciembre"]

    let sections = holdersItems.keys.compactMap({Int($0)}).sorted().compactMap { (keyInt) -> SectionItems? in
      guard let tours = holdersItems[String(keyInt)] else {
        return nil
      }

      let item = SectionItems(title: months[keyInt] ?? "Tours", itemType: .tour(tours: tours))
      item.itemPressHandler = pressHandler

      return item
    }

    return sections
  }

  func pressHandler(_ selectedItem: Any) {
    guard let tour = selectedItem as? Place, let navigationDelegate = navigationDelegate else {
      return
    }

    navigationDelegate.tourDetails(tour: tour)
  }
}
