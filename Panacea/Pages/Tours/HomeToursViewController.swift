//
//  HomeToursViewController.swift
//  Panacea
//
//  Created by Blashadow on 6/20/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class HomeToursViewController: BaseListToursViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func contentList() {
    let deadlineTime = DispatchTime.now() + .seconds(2)

    DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
      self?.requestFetchSession = PlaceDataHandler.shared.retrieveListOfTours { (_, tours) in
        guard let tours = tours else {
          return
        }

        self?.processItems(tours: tours)
      }
    }
  }
}
