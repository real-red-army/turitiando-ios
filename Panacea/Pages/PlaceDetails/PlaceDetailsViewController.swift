//
//  PlaceDetailsViewController.swift
//  Panacea
//
//  Created by Blashadow on 6/23/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke
import IGListKit
import Lightbox

protocol PlaceDetailsNavigationProtocol: AnyObject {
  func tourDetails(tour: Place)
  func forecastDetails(place: Place)
  func viewAllTours(tours: [Place])
  func dismiss()
}

class PlaceDetailsViewController: UIViewController {
  private let animateCollectionView = AnimatedCollectionView()

  weak var navigationDelegate: PlaceDetailsNavigationProtocol?
  var placeDetailsViewModel: PlaceDetailsViewModel?
  var placeDetailsView: PlaceDetailsView?
  var place: Place?

  lazy var adapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  lazy var tourAdapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  override func viewDidLoad() {
    super.viewDidLoad()

    let shouldShowToursContainer: Bool = self.place?.targets?.count ?? 0 > 1
    self.placeDetailsView = PlaceDetailsView(frame: .zero, enableToursContainer: shouldShowToursContainer)

    self.adapter.collectionView = self.placeDetailsView?.imageList
    self.adapter.dataSource = self
    self.adapter.collectionViewDelegate = self.animateCollectionView

    self.tourAdapter.collectionView = self.placeDetailsView?.tourList
    self.tourAdapter.dataSource = self
    self.tourAdapter.collectionViewDelegate = self.animateCollectionView

    self.placeDetailsView?.subscriptionImage.isHighlighted = isSubscriptionEnabled()
    self.placeDetailsView?.closeButton.tapHandler = closeViewController
    self.placeDetailsView?.weatherActionHandler = attempToWeatherDetails
    self.placeDetailsView?.shareTapHandler = shareDestination
    self.placeDetailsView?.subscriptionTapHandler = subscriptionPlace
    self.placeDetailsView?.viewAllToursTapHandler = viewAllTours

    // AutoLayout
    if let placeDetailsView = self.placeDetailsView {
      view.addSubview(placeDetailsView)

      NSLayoutConstraint.activate([
        placeDetailsView.widthAnchor.constraint(equalTo: view.widthAnchor),
        placeDetailsView.heightAnchor.constraint(equalTo: view.heightAnchor),
        placeDetailsView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        placeDetailsView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
      ])
    }

    self.setupData()
  }

  private func attempToWeatherDetails() {
    guard let navigationDelegate = self.navigationDelegate, let place = self.place else {
      return
    }

    navigationDelegate.forecastDetails(place: place)
  }

  private func setupData() {
    if let placeDetailsView = self.placeDetailsView, let place = place {
      placeDetailsViewModel = PlaceDetailsViewModel(place: place, view: placeDetailsView)
      placeDetailsViewModel?.setupData()

      let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openMap))

      placeDetailsView.mapImage.isUserInteractionEnabled = true
      placeDetailsView.mapImage.addGestureRecognizer(tapGesture)
    }
  }

  @objc private func openMap() {
    guard let place = self.place else {
      return
    }

    let controller = MapDetailsViewController()
    controller.place = place
    controller.modalPresentationStyle = .fullScreen

    self.navigationController?.pushViewController(controller, animated: true)
  }

  @objc func closeViewController() {
    if let navigationController = navigationController {
      navigationController.dismiss(animated: true, completion: nil)
    }

    // Call life cycle controller method
    dismiss(animated: true, completion: nil)
  }

  private func isSubscriptionEnabled() -> Bool {
    guard let placeIdentifier = self.place?.identifier else {
      return false
    }

    let dataAccess = DataAccess<SubscriptionItem>()
    let result = dataAccess.listOfItems().filter({ $0.placeId == Double(placeIdentifier) })

    return !result.isEmpty
  }

  private func subscriptionPlace() {
    guard let placeIdentifier = self.place?.identifier else {
      return
    }

    let generatedId = UUID().uuidString
    let subscrpitionItem = SubscriptionItem(generatedId, placeId: Double(placeIdentifier))

    let dataAccess = DataAccess<SubscriptionItem>()

    if isSubscriptionEnabled() {
      let dataAccess = DataAccess<SubscriptionItem>()
      let result = dataAccess.listOfItems().filter({ $0.placeId == Double(placeIdentifier) })

      dataAccess.deleteObjects(result)
    } else {
        dataAccess.saveItem(subscrpitionItem)

        DialogHelper.showMessage(controller: self,
                                 title: "Información",
                                 message: "Ahora recibirás notificaciones de tours relacionados a este destino!")
    }
  }

  private func shareDestination() {
    // text to share
    guard let shareMessage = self.place?.shareMessage else {
      return
    }

    share(content: shareMessage)
  }

  private func viewAllTours() {
    guard let navigationDelegate = self.navigationDelegate,
      let identifier = place?.identifier else {
      return
    }

    _ = PlaceDataHandler.shared.retrieveTours(with: identifier, completion: { (_, tours) in
        navigationDelegate.viewAllTours(tours: tours)
    })
  }

  private func tourPressHandler(_ tile: DisplayableTile) {
    guard let navigationDelegate = self.navigationDelegate else {
      return
    }

    let tourIdentifier = tile.identifier

    _ = PlaceDataHandler
      .shared
      .retrievePlaceInfo(with: tourIdentifier) { [navigationDelegate](_, tour) in
      guard let tour = tour else {
        return
      }

      navigationDelegate.tourDetails(tour: tour)
    }
  }

  private func pressImageHandler(_ placeImage: PlaceImage) {
    LightBoxViewController.presentLightBoxController(self, images: place?.images ?? [])
  }

  override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
    super.dismiss(animated: flag, completion: completion)

    guard let navigationDelegate = navigationDelegate else {
      return
    }

    navigationDelegate.dismiss()
  }
}

extension PlaceDetailsViewController: ListAdapterDataSource {
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {

    if (listAdapter == adapter) {
      return self.place?.images ?? []
    } else if listAdapter == tourAdapter {
      if let targets = self.place?.targets {
        if targets.count > 1 {
          return Array(targets.prefix(upTo: 2))
        } else {
          return targets
        }
      }
    }

    return []
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    if self.tourAdapter == listAdapter {
      return TourTileController(with: self.tourPressHandler, style: .fullWidth)
    }

    return GenericListSectionController(pressHandler: pressImageHandler, displayableItem: ImageDisplayableCell())
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}

extension PlaceDetailsViewController: ShareableController {}
