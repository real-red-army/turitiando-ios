//
//  WeatherCard.swift
//  Panacea
//
//  Created by Blashadow on 7/14/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class WeatherCard: UIView {
  let weatherImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = UIColor(hexValue: Colors.WeatherBackground)
    view.isOpaque = true
    view.clipsToBounds = true

    return view
  }()

  let labelContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.clipsToBounds = true
    view.backgroundColor = UIColor(hexValue: Colors.WeatherBackground)

    return view
  }()

  let statusLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.text = ""
    label.textColor = UIColor.white
    label.backgroundColor = UIColor(hexValue: Colors.WeatherBackground)
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
    label.clipsToBounds = true

    return label
  }()

  let currentTemperatureLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 40)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.text = "22 °C"
    label.textColor = UIColor.white
    label.backgroundColor = UIColor(hexValue: Colors.WeatherBackground)
    label.clipsToBounds = true
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupConstraints()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupConstraints() {
    backgroundColor = UIColor(hexValue: Colors.WeatherBackground)
    translatesAutoresizingMaskIntoConstraints = false

    addSubview(weatherImage)

    NSLayoutConstraint.activate([
      weatherImage.widthAnchor.constraint(equalToConstant: 100),
      weatherImage.heightAnchor.constraint(equalToConstant: 100),
      weatherImage.centerYAnchor.constraint(equalTo: centerYAnchor),
      weatherImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 10)
    ])

    addSubview(labelContainer)

    NSLayoutConstraint.activate([
      labelContainer.centerYAnchor.constraint(equalTo: weatherImage.centerYAnchor),
      labelContainer.heightAnchor.constraint(greaterThanOrEqualToConstant: 30),
      labelContainer.rightAnchor.constraint(equalTo: rightAnchor),
      labelContainer.leftAnchor.constraint(equalTo: weatherImage.rightAnchor, constant: 10)
    ])

    labelContainer.addSubview(statusLabel)

    NSLayoutConstraint.activate([
      statusLabel.topAnchor.constraint(equalTo: labelContainer.topAnchor),
      statusLabel.leftAnchor.constraint(equalTo: labelContainer.leftAnchor, constant: 10),
      statusLabel.rightAnchor.constraint(equalTo: labelContainer.rightAnchor),
      statusLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 10)
    ])

    labelContainer.addSubview(currentTemperatureLabel)

    NSLayoutConstraint.activate([
      currentTemperatureLabel.topAnchor.constraint(equalTo: statusLabel.bottomAnchor),
      currentTemperatureLabel.leftAnchor.constraint(equalTo: statusLabel.leftAnchor),
      currentTemperatureLabel.rightAnchor.constraint(equalTo: labelContainer.rightAnchor),
      currentTemperatureLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      currentTemperatureLabel.bottomAnchor.constraint(equalTo: labelContainer.bottomAnchor)
    ])
  }
}
