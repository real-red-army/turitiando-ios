//
//  PlaceDetailsView.swift
//  Panacea
//
//  Created by Blashadow on 7/13/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class PlaceDetailsView: UIView {
  var subscriptionTapHandler: (() -> Void)?
  var favoriteTapHandler: (() -> Void)?
  var viewAllToursTapHandler: (() -> Void)?
  var shareTapHandler: (() -> Void)?

  var scrollView: UIScrollView = {
    let view = UIScrollView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layer.shouldRasterize = true
    view.showsVerticalScrollIndicator = false
    view.layer.rasterizationScale = UIScreen.main.scale

    return view
  }()

  var closeButton = CircleCloseButton(frame: .zero)

  var placeImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true

    return view
  }()

  var favoriteImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true
    view.isUserInteractionEnabled = true
    view.image = UIImage(named: "FavoriteIconOutline")
    view.highlightedImage = UIImage(named: "FavoriteIconFilled")

    return view
  }()

  var shareImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true
    view.isUserInteractionEnabled = true
    view.image = UIImage(named: "ShareIcon")

    return view
  }()

  var subscriptionImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true
    view.isUserInteractionEnabled = true
    view.image = UIImage(named: "subscription_empty")
    view.highlightedImage = UIImage(named: "subscription_full")

    return view
  }()

  var placeImageContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var placeTitle: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 18)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.clipsToBounds = true
    label.translatesAutoresizingMaskIntoConstraints = false
    label.layer.shouldRasterize = true
    label.layer.rasterizationScale = UIScreen.main.scale

    return label
  }()

  var placeDetails: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 14)
    label.textColor = UIColor(hexValue: Colors.Description)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.clipsToBounds = true
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  var imageList: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal

    let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layoutMargins = .zero
    view.backgroundColor = UIColor.white
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false

    return view
  }()

  var tourList: UICollectionView = {
    let layout = UICollectionViewFlowLayout()

    let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layoutMargins = .zero
    view.backgroundColor = UIColor.white
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false

    return view
  }()

  var tourContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var availableToursLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 16)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Tours disponibles"

    return label
  }()

  var viewAllToursLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 13)
    label.textColor = UIColor(hexValue: Colors.SubTitle)
    label.textAlignment = .right
    label.numberOfLines = 0
    label.isUserInteractionEnabled = true
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Ver todos"

    return label
  }()

  var weatherActionHandler: (() -> Void)?

  var weatherContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var placeWeatherLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 16)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Clima"

    return label
  }()

  var weatherContent = WeatherCard(frame: .zero)

  var forecastLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 12)
    label.textColor = UIColor(hexValue: Colors.SubTitle)
    label.textAlignment = .right
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Pronostico"

    return label
  }()

  var mapImageContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var mapTitle: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 16)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Ubicacion"

    return label
  }()

  var mapImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true

    return view
  }()

  private var toursContainerHeightConstraint: NSLayoutConstraint?

  init(frame: CGRect, enableToursContainer: Bool) {
    super.init(frame: frame)

    self.translatesAutoresizingMaskIntoConstraints = false

    self.setupViews(enableToursContainer)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  fileprivate func setupCloseButtonConstraint() {
    scrollView.addSubview(closeButton)

    NSLayoutConstraint.activate([
      closeButton.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 15),
      closeButton.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 15)
    ])

    let constraint = closeButton.topAnchor.constraint(lessThanOrEqualTo: self.topAnchor, constant: 40)
    constraint.priority = UILayoutPriority.required
    constraint.isActive = true
  }

  @objc func weatherAction() {
    if let handler = weatherActionHandler {
      handler()
    }
  }

  @objc func tapImageAction() {
    if let handler = favoriteTapHandler {
      handler()
    } else {
      favoriteImage.isHighlighted = !favoriteImage.isHighlighted
    }
  }

  @objc func tapShareImageAction() {
    if let handler = shareTapHandler {
      handler()
    }
  }

  @objc func tapSubscriptionAction() {
    if let handler = subscriptionTapHandler {
      handler()

      // Toogle states
      subscriptionImage.isHighlighted = !subscriptionImage.isHighlighted
    }
  }
}

extension PlaceDetailsView {
  private func setupViews(_ enableToursContainer: Bool) {
    backgroundColor = UIColor.white
    addSubview(scrollView)

    NSLayoutConstraint.activate([
      scrollView.widthAnchor.constraint(equalTo: widthAnchor),
      scrollView.heightAnchor.constraint(equalTo: heightAnchor),
      scrollView.centerXAnchor.constraint(equalTo: centerXAnchor),
      scrollView.centerYAnchor.constraint(equalTo: centerYAnchor)
    ])

    scrollView.addSubview(placeImageContainer)
    placeImageContainer.addSubview(placeImage)
    setupImageContainer()

    setupCloseButtonConstraint()

    scrollView.addSubview(placeTitle)
    scrollView.addSubview(shareImage)
    scrollView.addSubview(subscriptionImage)

    NSLayoutConstraint.activate([
      shareImage.widthAnchor.constraint(equalToConstant: 30),
      shareImage.heightAnchor.constraint(equalToConstant: 30),
      shareImage.centerYAnchor.constraint(equalTo: placeTitle.centerYAnchor),
      shareImage.rightAnchor.constraint(equalTo: placeImageContainer.rightAnchor, constant: -10)
    ])

    shareImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapShareImageAction)))

    NSLayoutConstraint.activate([
      subscriptionImage.widthAnchor.constraint(equalToConstant: 30),
      subscriptionImage.heightAnchor.constraint(equalToConstant: 30),
      subscriptionImage.centerYAnchor.constraint(equalTo: placeTitle.centerYAnchor),
      subscriptionImage.rightAnchor.constraint(equalTo: shareImage.leftAnchor, constant: -10)
    ])

    let gesture = UITapGestureRecognizer(target: self, action: #selector(tapSubscriptionAction))
    subscriptionImage.addGestureRecognizer(gesture)

    NSLayoutConstraint.activate([
      placeTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: 20),
      placeTitle.topAnchor.constraint(equalTo: placeImageContainer.bottomAnchor, constant: 10),
      placeTitle.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 10),
      placeTitle.rightAnchor.constraint(equalTo: subscriptionImage.leftAnchor, constant: 5)
    ])

    scrollView.addSubview(placeDetails)

    NSLayoutConstraint.activate([
      placeDetails.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0, constant: -20),
      placeDetails.heightAnchor.constraint(greaterThanOrEqualToConstant: 50),
      placeDetails.topAnchor.constraint(equalTo: placeTitle.bottomAnchor, constant: 10),
      placeDetails.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    scrollView.addSubview(imageList)

    NSLayoutConstraint.activate([
      imageList.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
      imageList.heightAnchor.constraint(greaterThanOrEqualToConstant: 100),
      imageList.topAnchor.constraint(equalTo: placeDetails.bottomAnchor, constant: 10),
      imageList.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    scrollView.addSubview(tourContainer)
    setupTourContainerContent(enableToursContainer)

    scrollView.addSubview(weatherContainer)
    setupWeatherContainer()

    scrollView.addSubview(mapImageContainer)
    setupMapImageContainer()
  }

  // MARK: - Setup map image
  private func setupMapImageContainer() {
    NSLayoutConstraint.activate([
      mapImageContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
      mapImageContainer.heightAnchor.constraint(greaterThanOrEqualToConstant: 250),
      mapImageContainer.topAnchor.constraint(equalTo: weatherContainer.bottomAnchor, constant: 20),
      mapImageContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
      mapImageContainer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -50)
    ])

    mapImageContainer.addSubview(mapTitle)

    NSLayoutConstraint.activate([
      mapTitle.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
      mapTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      mapTitle.leftAnchor.constraint(equalTo: mapImageContainer.leftAnchor, constant: 5),
      mapTitle.topAnchor.constraint(equalTo: mapImageContainer.topAnchor)
    ])

    mapImageContainer.addSubview(mapImage)

    NSLayoutConstraint.activate([
      mapImage.widthAnchor.constraint(equalTo: mapImageContainer.widthAnchor),
      mapImage.heightAnchor.constraint(equalToConstant: 250),
      mapImage.centerXAnchor.constraint(equalTo: mapImageContainer.centerXAnchor),
      mapImage.topAnchor.constraint(equalTo: mapTitle.bottomAnchor, constant: 10),
      mapImage.bottomAnchor.constraint(equalTo: mapImageContainer.bottomAnchor)
    ])
  }

  private func setupImageContainer() {
    NSLayoutConstraint.activate([
      placeImageContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
      placeImageContainer.heightAnchor.constraint(equalToConstant: 250),
      placeImageContainer.topAnchor.constraint(equalTo: scrollView.topAnchor),
      placeImageContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    let constraint = placeImage.topAnchor.constraint(lessThanOrEqualTo: self.topAnchor)
    constraint.priority = UILayoutPriority.defaultHigh

    let heightConstraint = placeImage.heightAnchor.constraint(
      greaterThanOrEqualTo: placeImageContainer.heightAnchor
    )
    constraint.priority = UILayoutPriority.required

    NSLayoutConstraint.activate([
      placeImage.widthAnchor.constraint(equalTo: placeImageContainer.widthAnchor),
      constraint,
      heightConstraint,
      placeImage.bottomAnchor.constraint(equalTo: placeImageContainer.bottomAnchor),
      placeImage.centerXAnchor.constraint(equalTo: placeImageContainer.centerXAnchor)
    ])
  }

  // MARK: - Weather card setup
  private func setupWeatherContainer() {
    NSLayoutConstraint.activate([
      weatherContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
      weatherContainer.heightAnchor.constraint(greaterThanOrEqualToConstant : 125),
      weatherContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
      weatherContainer.topAnchor.constraint(equalTo: tourContainer.bottomAnchor, constant: 10)
    ])

    weatherContainer.addSubview(placeWeatherLabel)

    NSLayoutConstraint.activate([
      placeWeatherLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
      placeWeatherLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      placeWeatherLabel.leftAnchor.constraint(equalTo: weatherContainer.leftAnchor, constant: 5),
      placeWeatherLabel.topAnchor.constraint(equalTo: weatherContainer.topAnchor)
    ])

    weatherContainer.addSubview(forecastLabel)

    NSLayoutConstraint.activate([
      forecastLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
      forecastLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      forecastLabel.rightAnchor.constraint(equalTo: weatherContainer.rightAnchor, constant: -5),
      forecastLabel.centerYAnchor.constraint(equalTo: placeWeatherLabel.centerYAnchor)
    ])

    weatherContainer.addSubview(weatherContent)

    NSLayoutConstraint.activate([
      weatherContent.widthAnchor.constraint(equalTo: weatherContainer.widthAnchor),
      weatherContent.heightAnchor.constraint(equalToConstant: 120),
      weatherContent.topAnchor.constraint(equalTo: placeWeatherLabel.bottomAnchor, constant: 10),
      weatherContent.centerXAnchor.constraint(equalTo: weatherContainer.centerXAnchor),
      weatherContent.bottomAnchor.constraint(equalTo: weatherContainer.bottomAnchor)
    ])

    weatherContainer.isUserInteractionEnabled = true
    weatherContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(weatherAction)))
  }

  private func setupTourContainerContent(_ enableToursContainer: Bool) {
    if enableToursContainer == false {
      tourContainer.isHidden = true

      NSLayoutConstraint.activate([
        tourContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
        tourContainer.heightAnchor.constraint(equalToConstant: 0),
        tourContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
        tourContainer.topAnchor.constraint(equalTo: imageList.bottomAnchor, constant: 10)
      ])

      return
    }

    NSLayoutConstraint.activate([
      tourContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
      tourContainer.heightAnchor.constraint(greaterThanOrEqualToConstant: 300),
      tourContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
      tourContainer.topAnchor.constraint(equalTo: imageList.bottomAnchor, constant: 10)
    ])

    tourContainer.addSubview(availableToursLabel)

    NSLayoutConstraint.activate([
      availableToursLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
      availableToursLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      availableToursLabel.leftAnchor.constraint(equalTo: tourContainer.leftAnchor, constant: 5),
      availableToursLabel.topAnchor.constraint(equalTo: tourContainer.topAnchor)
    ])

    viewAllToursLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewAllTours)))

    tourContainer.addSubview(viewAllToursLabel)

    NSLayoutConstraint.activate([
      viewAllToursLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
      viewAllToursLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      viewAllToursLabel.rightAnchor.constraint(equalTo: tourContainer.rightAnchor, constant: -5),
      viewAllToursLabel.centerYAnchor.constraint(equalTo: availableToursLabel.centerYAnchor)
    ])

    tourContainer.addSubview(tourList)

    NSLayoutConstraint.activate([
      tourList.widthAnchor.constraint(equalTo: tourContainer.widthAnchor, multiplier: 1.0),
      tourList.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      tourList.topAnchor.constraint(equalTo: viewAllToursLabel.topAnchor, constant: 20),
      tourList.bottomAnchor.constraint(equalTo: tourContainer.bottomAnchor),
      tourList.leftAnchor.constraint(equalTo: tourContainer.leftAnchor)
    ])
  }

  @objc private func viewAllTours() {
    if let handler = viewAllToursTapHandler {
      handler()
    }
  }
}
