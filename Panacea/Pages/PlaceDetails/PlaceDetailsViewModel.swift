//
//  PlaceDetailsViewModel.swift
//  Panacea
//
//  Created by Blashadow on 7/20/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke

class PlaceDetailsViewModel {
  private unowned let place: Place
  private unowned let placeDetailsView: PlaceDetailsView

  init(place value: Place, view: PlaceDetailsView) {
    self.place = value
    self.placeDetailsView = view
  }

  func setupData() {
    placeDetailsView.placeTitle.text = place.placeTitle
    placeDetailsView.placeDetails.text = place.placeDescription
    placeDetailsView.weatherContent.currentTemperatureLabel.text = "\(place.weather?.currentTemperature ?? 0) °C"
    placeDetailsView.weatherContent.statusLabel.text = place.weather?.status

    if let imageURL = URL(string: place.placeImageHeaderUrl ?? "") {
      Nuke.loadImage(with: imageURL, into: placeDetailsView.placeImage)
    }

    let cleanUrlString = place.mapUrl?
      .addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

    if let mapURL = URL(string: cleanUrlString ?? "") {
      Nuke.loadImage(with: mapURL, into: placeDetailsView.mapImage)
    }

    if let iconWeatherUrl = URL(string: place.weather?.iconUrl ?? "") {
      Nuke.loadImage(with: iconWeatherUrl, into: placeDetailsView.weatherContent.weatherImage)
    }
  }
}
