//
//  PlaceDetailsCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/1/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class PlaceDetailsCoordinator: Coordinator {
  var dismissCoordinator: (() -> Void)?

  private let place: Place
  private let navigator: UIViewController

  private var placeDetailsController: PlaceDetailsViewController?
  private var forecastCoordinator: ForecastDetailsCoordinator?
  private var tourDetailsCoordinator: TourDetailsCoordinator?
  private var tourListCoordinator: TourListCoordinator?

  init(place: Place, navigator: UIViewController) {
    self.place = place
    self.navigator = navigator
  }

  func start() {
    placeDetailsController = PlaceDetailsViewController()
    placeDetailsController?.navigationDelegate = self
    placeDetailsController?.place = place

    let button = UIBarButtonItem(title: "Cancelar",
                                 style: .plain,
                                target: placeDetailsController,
                                action: #selector(placeDetailsController?.closeViewController))

    guard let controller = placeDetailsController else {
      return
    }

    let navigationController = UINavigationController(rootViewController: controller)
    navigationController.topViewController?.navigationItem.title = "Place details"
    navigationController.topViewController?.navigationItem.leftBarButtonItem = button
    navigationController.modalPresentationStyle = .fullScreen
    navigationController.isNavigationBarHidden = true

    self.navigator.present(navigationController, animated: true, completion: nil)
  }
}

extension PlaceDetailsCoordinator: PlaceDetailsNavigationProtocol {
  func tourDetails(tour: Place) {
    guard let controller = self.placeDetailsController else {
      return
    }

    self.tourDetailsCoordinator = TourDetailsCoordinator(tour: tour, navigator: controller)
    self.tourDetailsCoordinator?.start()
    self.tourDetailsCoordinator?.dismissCoordinator = self.onDismissTourDetailsCoordinator
  }

  func forecastDetails(place: Place) {
    guard let controller = placeDetailsController else {
      return
    }

    forecastCoordinator = ForecastDetailsCoordinator(navigator: controller, place: place)
    forecastCoordinator?.start()
    forecastCoordinator?.onDismissController = onDismissForecastDetails
  }

  private func onDismissTourDetailsCoordinator() {
    self.tourDetailsCoordinator = nil
  }

  private func onDismissForecastDetails() {
    forecastCoordinator = nil
  }

  func viewAllTours(tours: [Place]) {
    guard let controller = placeDetailsController else {
      return
    }

    tourListCoordinator = TourListCoordinator(tours: tours, navigator: controller)
    tourListCoordinator?.dismissCoordinator = dismissTourListCoordinator
    tourListCoordinator?.start()
  }

  func dismissTourListCoordinator() {
    tourListCoordinator = nil
  }

  func dismiss() {
    placeDetailsController = nil

    guard let dismissCoordinator = self.dismissCoordinator else {
      return
    }

    dismissCoordinator()
  }
}
