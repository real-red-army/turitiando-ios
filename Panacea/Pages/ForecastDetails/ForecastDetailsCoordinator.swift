//
//  WeatherDetailsCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/3/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class ForecastDetailsCoordinator: Coordinator {
  var dismissCoordinator: (() -> Void)?
  
  private let navigator: UIViewController
  private let place: Place

  var onDismissController: (() -> Void)?

  init(navigator: UIViewController, place: Place) {
    self.navigator = navigator
    self.place = place
  }

  func start() {
    let controller = ForecastDetailsViewController()
    controller.place = place
    controller.modalPresentationStyle = .fullScreen

    self.navigator.present(controller, animated: true, completion: nil)
  }
}

extension ForecastDetailsCoordinator: ForecastDetailsNavigationDelegate {
  func dismiss() {
    if let handler = onDismissController {
      handler()
    }
  }
}
