//
//  ForecastDetailsViewController.swift
//  Panacea
//
//  Created by Luis Romero on 11/3/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit
import Nuke

protocol ForecastDetailsNavigationDelegate : NavigationDelegate {
}

class ForecastDetailsViewController: UIViewController {
  private let animateCollectionView = AnimatedCollectionView()
  private var navigationDelegate: ForecastDetailsNavigationDelegate?

  lazy var adapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self, workingRangeSize: 0)
  }()

  var place: Place?
  var detailsView: ForecastDetailsView?

  override func viewDidLoad() {
    super.viewDidLoad()

    let customView = ForecastDetailsView(frame: .zero)
    self.detailsView = customView
    self.detailsView?.closeButton.tapHandler = closeViewController

    adapter.collectionView = customView.collectionView
    adapter.collectionViewDelegate = animateCollectionView
    adapter.dataSource = self

    view.addSubview(customView)

    NSLayoutConstraint.activate([
      customView.widthAnchor.constraint(equalTo: view.widthAnchor),
      customView.heightAnchor.constraint(equalTo: view.heightAnchor),
      customView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      customView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
    ])

    if let imageUrl = URL(string: place?.imageHeaderUrl ?? ""),
      let imageView = self.detailsView?.placeImage {
      Nuke.loadImage(with: imageUrl, into: imageView)
    }

    detailsView?.weatherCard.statusLabel.text = place?.weather?.status

    if let imageUrl = URL(string: place?.weather?.iconUrl ?? ""),
      let imageView = self.detailsView?.weatherCard.weatherImage {
      Nuke.loadImage(with: imageUrl, into: imageView)
    }
  }

  private func forecastPressHandler(_ item: ForecastItem) {
    // Do nothing
  }

  private func closeViewController() {
    dismiss(animated: true, completion: nil)
  }

  override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
    super.dismiss(animated: flag, completion: completion)

    guard let navigationDelegate = navigationDelegate else {
      return
    }

    navigationDelegate.dismiss()
  }
}

extension ForecastDetailsViewController: ListAdapterDataSource {
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    let items = place?.weather?.forecast ?? []

    return items
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    return GenericListSectionController<ForecastItem>(pressHandler: forecastPressHandler,
                                                      displayableItem: ForecastItemDisplayableCell())
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}
