//
//  ForecastDetailsView.swift
//  Panacea
//
//  Created by Luis Romero on 11/3/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class ForecastDetailsView: UIView {
  let closeButton = CircleCloseButton(frame: .zero)

  let scrollView: UIScrollView = {
    let view = UIScrollView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.showsVerticalScrollIndicator = false

    return view
  }()

  let weatherCard: WeatherCard = {
    let view = WeatherCard(frame: .zero)
    view.backgroundColor = UIColor.clear

    view.weatherImage.backgroundColor = UIColor.clear
    view.statusLabel.backgroundColor = UIColor.clear
    view.currentTemperatureLabel.backgroundColor = UIColor.clear
    view.labelContainer.backgroundColor = UIColor.clear

    return view
  }()

  var placeImage: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = UIViewContentMode.scaleAspectFill
    view.clipsToBounds = true

    return view
  }()

  let headerContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  let currentWeatherContainer: UIView = {
    let view = UIView(frame: .zero)
    view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  let collectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .vertical

    let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layoutMargins = .zero
    view.backgroundColor = UIColor.white
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false
    view.isScrollEnabled = false

    return view
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    self.translatesAutoresizingMaskIntoConstraints = false

    setupView()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupView() {
    addSubview(scrollView)

    NSLayoutConstraint.activate([
      scrollView.widthAnchor.constraint(equalTo: widthAnchor),
      scrollView.heightAnchor.constraint(equalTo: heightAnchor),
      scrollView.centerYAnchor.constraint(equalTo: centerYAnchor),
      scrollView.centerXAnchor.constraint(equalTo: centerXAnchor)
    ])

    scrollView.addSubview(headerContainer)

    NSLayoutConstraint.activate([
      headerContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
      headerContainer.heightAnchor.constraint(equalToConstant: 200),
      headerContainer.topAnchor.constraint(equalTo: scrollView.topAnchor),
      headerContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
    ])

    scrollView.addSubview(collectionView)

    NSLayoutConstraint.activate([
      collectionView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
      collectionView.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
      collectionView.topAnchor.constraint(equalTo: headerContainer.bottomAnchor, constant: 0),
      collectionView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
      collectionView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
    ])

    headerContainer.addSubview(placeImage)
    elasticViewWithParent(parent: headerContainer, view: placeImage)

    headerContainer.addSubview(currentWeatherContainer)
    elasticViewWithParent(parent: headerContainer, view: currentWeatherContainer)

    currentWeatherContainer.addSubview(weatherCard)

    NSLayoutConstraint.activate([
      weatherCard.widthAnchor.constraint(equalTo: currentWeatherContainer.widthAnchor, multiplier: 0.8),
      weatherCard.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      weatherCard.bottomAnchor.constraint(equalTo: currentWeatherContainer.bottomAnchor, constant: -90),
      weatherCard.centerXAnchor.constraint(equalTo: currentWeatherContainer.centerXAnchor)
    ])

    setupCloseButtonConstraint()
  }

  private func elasticViewWithParent(parent: UIView, view: UIView) {
    parent.addSubview(view)

    let constraint = view.topAnchor.constraint(lessThanOrEqualTo: self.topAnchor)
    constraint.priority = UILayoutPriority.defaultHigh

    let heightConstraint = view.heightAnchor.constraint(
      greaterThanOrEqualTo: parent.heightAnchor
    )
    constraint.priority = UILayoutPriority.required

    NSLayoutConstraint.activate([
      view.widthAnchor.constraint(equalTo: parent.widthAnchor),
      constraint,
      heightConstraint,
      view.bottomAnchor.constraint(equalTo: parent.bottomAnchor),
      view.centerXAnchor.constraint(equalTo: parent.centerXAnchor)
    ])

  }

  fileprivate func setupCloseButtonConstraint() {
    scrollView.addSubview(closeButton)

    NSLayoutConstraint.activate([
      closeButton.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 15),
      closeButton.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 15)
    ])

    let constraint = closeButton.topAnchor.constraint(lessThanOrEqualTo: self.topAnchor, constant: 30)
    constraint.priority = UILayoutPriority.required
    constraint.isActive = true
  }
}
