//
//  SectionItems
//  Panacea
//
//  Created by Blashadow on 6/20/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

enum ItemType {
  case placeTile(places: [DisplayableTile])
  case place(places: [Place])
  case tourTile(tours: [DisplayableTile])
  case paths(paths: [DisplayableTile])
  case tour(tours: [Place])
  case notification(notifications: [RemoteNotification])
  case loading(dummyItems: [Any])

  var value: Any {
    switch self {
    case .placeTile(let places): return places
    case .place(let places): return places
    case .tourTile(let tours): return tours
    case .notification(let notifications): return notifications
    case .tour(let tours): return tours
    case .paths(let paths): return paths
    case .loading(let items): return items
    }
  }
}

class SectionItems: NSObject {
  let title: String
  let itemType: ItemType
  var headerPressHandler: (() -> Void)?
  var itemPressHandler: ((_ item: Any) -> Void)?

  init(title: String, itemType: ItemType) {
    self.title = title
    self.itemType = itemType
  }
}

extension SectionItems: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return String(format: "\(self.hash)") as NSString
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? SectionItems else {
      return false
    }

    return self.title == object.title
  }
}
