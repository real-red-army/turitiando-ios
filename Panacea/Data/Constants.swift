//
//  Constants.swift
//  Panacea
//
//  Created by Blashadow on 6/20/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

enum WeekDays: Int {
  case domingo = 1, lunes, martes, miercoles, jueves, viernes, sabado

  func day() -> String {
    switch self {
    case .domingo:
      return "Domingo"
    case .lunes:
      return "Lunes"
    case .martes:
      return "Marte"
    case .miercoles:
      return "Miercoles"
    case .jueves:
      return "Jueves"
    case .viernes:
      return "Viernes"
    case .sabado:
      return "Sabado"
    }
  }
}

struct FontNames {
  static let Light = "Montserrat-Light"
  static let Medium = "Montserrat-Medium"
  static let Regular = "Montserrat-Regular"
  static let SemiBold = "Montserrat-SemiBold"
}

struct Colors {
  static let Text = 0x333
  static let Title = 0x39424C
  static let SubTitle = 0xA6BBCF
  static let Description = 0x586069
  static let WeatherBackground = 0x1E88E5
  static let LinkColor = 0x2962FF
  static let LightGray = 0xF0F2F8
  static let LighterGray = 0xE4E6EC
  static let Gray = 0xF0F0F0
  static let ActionColor = 0x1E88E5
}

struct NotificationImage {
  static let Default = "NotificationDefault"
  static let Tour = "NotificationTour"
  static let Place = "NotificationPlace"
  static let Weekly = "NotificationWeekly"
}
