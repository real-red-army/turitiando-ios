//
//  DataAccess.swift
//  Panacea
//
//  Created by Luis Romero on 10/13/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import RealmSwift

class DataAccess<T: Object>: NSObject {
  var database: Realm? {
    do {
      return try Realm()
    } catch {
      print("Error", error)
      return nil
    }
  }

  func saveItem(_ item: T) {
    do {
      try database?.write {
        database?.add(item)
      }
    } catch {
      print("Error writing into the database", error)
    }
  }

  func listOfItems() -> [T] {
    guard let database = database else {
      return []
    }

    let results = database.objects(T.self)

    return Array(results)
  }

  func deleteObjects(_ items: [T]) {
    do {
      try database?.write {
        database?.delete(items)
      }
    } catch {
      print("Error writing into the database", error)
    }
  }
}
