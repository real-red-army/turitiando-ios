//
//  PlaceDataHandler.swift
//  Panacea
//
//  Created by Blashadow on 6/15/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class PlaceDataHandler: NSObject {
  static let shared: PlaceDataHandler = {
    return PlaceDataHandler()
  }()

  override private init() {
  }

  public func retrieveListOfPlaces(with completion: @escaping (Error?, [Place]?) -> Void) -> URLSessionDataTask? {
    //Class places
    let wsClient = WebServiceClient()

    let session = wsClient.retreivePlaces(with: { (_, items) in
      let places = items?.map(DataParser.placeWith)

      completion(nil, places)
    })

    return session
  }

  public func retrieveListOfTours(with completion: @escaping (Error?, [Place]?) -> Void) -> URLSessionDataTask? {
    //Class places
    let wsClient = WebServiceClient()

    let session = wsClient.retreiveTours(with: { (_, items) in
      let places = items?.map(DataParser.placeWith)

      completion(nil, places)
    })

    return session
  }

  public func retrievePlaceInfo(with identifier: Int,
                                completion: @escaping (Error?, Place?) -> Void) -> URLSessionDataTask? {
    let wsClient = WebServiceClient()

    let session = wsClient.placeDetails(with: identifier) { (error, values) in
      guard let values = values else {
        completion(error, nil)
        return
      }

      let place = DataParser.placeWith(rawData: values)

      completion(error, place)
    }

    return session
  }

  public func retrieveTours(with placeIdentifier: Int,
                            completion: @escaping (Error?, [Place]) -> Void) -> URLSessionDataTask? {
    let wsClient = WebServiceClient()

    let session = wsClient.retieveTourFromPlace(placeIdentifier: placeIdentifier) { (error, values) in
      let tours = values?.map(DataParser.placeWith)

      completion(error, tours ?? [])
    }

    return session
  }
}
