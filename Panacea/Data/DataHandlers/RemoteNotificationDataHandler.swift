//
//  RemoteNotificationDataHandler.swift
//  Panacea
//
//  Created by Luis Romero on 10/12/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class RemoteNotificationDataHandler: NSObject {
  static let shared: RemoteNotificationDataHandler = {
    return RemoteNotificationDataHandler()
  }()

  override private init() {
  }

  public func signupForNotification(withDevice deviceIdentifier: String,
                                    token: String,
                                    completion: @escaping (Error?) -> Void) -> URLSessionDataTask? {
    //Class places
    let wsClient = WebServiceClient()

    let session = wsClient.signupForNotification(with: deviceIdentifier, token: token) { (_, _) in
      completion(nil)
    }

    return session
  }
}
