//
//  PathRequestDataHandler.swift
//  Panacea
//
//  Created by Luis Romero on 10/13/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class PathRequestDataHandler: NSObject {
  static let shared: PathRequestDataHandler = {
    return PathRequestDataHandler()
  }()

  override private init() {
  }

  public func retrieveListOfPaths(with completion: @escaping (Error?, [Path]?) -> Void) -> URLSessionDataTask? {
    //Class places
    let wsClient = WebServiceClient()

    let session = wsClient.paths(completion: { (_, items) in
      let paths = items?.map(DataParser.path)

      completion(nil, paths)
    })

    return session
  }
}
