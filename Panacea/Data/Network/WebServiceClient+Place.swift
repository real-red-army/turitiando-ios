//
//  WebServiceClient+Place.swift
//  Panacea
//
//  Created by Luis Romero on 10/13/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

extension WebServiceClient {
  func placeDetails(with identifier: Int, completion: @escaping DetailsCompletion) -> URLSessionDataTask? {
    let urlRoute = String(format: "%@place/%d", baseServiceUrl, identifier)

    return HTTPHandler.performGetRequest(urlRoute, completion: completion)
  }

  func retreivePlaces(with completion: @escaping ListCompletion) -> URLSessionDataTask? {
    let urlRoute = String(format: "%@places/", baseServiceUrl)

    return HTTPHandler.performGetRequest(urlRoute, completion: completion)
  }

  func retreiveTours(with completion: @escaping ListCompletion) -> URLSessionDataTask? {
    let urlRoute = String(format: "%@tours/1/100/", baseServiceUrl)

    return HTTPHandler.performGetRequest(urlRoute, completion: completion)
  }

  func retieveTourFromPlace(placeIdentifier: Int, completion: @escaping ListCompletion) -> URLSessionDataTask? {
    let urlRoute = String(format: "%@place/%d/tours/", baseServiceUrl, placeIdentifier)

    return HTTPHandler.performGetRequest(urlRoute, completion: completion)
  }
}
