//
//  HTTPHandler.swift
//  Panacea
//
//  Created by Luis Romero on 10/13/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class HTTPHandler: NSObject {
  static func performPostRequest<T>(_ urlRoute: String,
                                    data: [String: Any],
                                    completion:@escaping (Error?, T?) -> Void) -> URLSessionDataTask? {
    guard let urlRequest = URL(string: urlRoute) else {
      return nil
    }

    do {
      let request = NSMutableURLRequest(url: urlRequest)
      request.httpMethod = "POST"
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      request.httpBody = try JSONSerialization.data(withJSONObject: data, options: [])

      return performRequest(request, completion: completion)
    } catch {
      return nil
    }
  }

  static func performGetRequest<T>(_ urlRoute: String,
                                   completion:@escaping (Error?, T?) -> Void) -> URLSessionDataTask? {
    guard let urlRequest = URL(string: urlRoute) else {
      return nil
    }

    let request = NSMutableURLRequest(url: urlRequest)
    request.httpMethod = "GET"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")

    return performRequest(request, completion: completion)
  }

  static func performRequest<T>(_ request: NSMutableURLRequest,
                                completion:@escaping (Error?, T?) -> Void) -> URLSessionDataTask? {
    let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, _, _) in
      guard let data = data else { return }

      let json = try? JSONSerialization.jsonObject(with: data, options: [])
      let results = json as? [String: Any]

      DispatchQueue.main.async {
        let result: T? = results?["result"] as? T

        completion(nil, result)
      }
    }

    task.resume()

    return task
  }
}
