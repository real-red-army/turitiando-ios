//
//  WebServiceClient.swift
//  Panacea
//
//  Created by Blashadow on 10/20/18.
//  Copyright © 2018 blashadow. All rights reserved.
//

import UIKit

typealias ListCompletion = (_ error: Error?, _ items: [[String: Any]]?) -> Void

typealias DetailsCompletion = (_ error: Error?, _ item: [String: Any]?) -> Void

class WebServiceClient: NSObject {
  let baseServiceUrl = "https://turitiando.com/api/rest/v1/"

  func signupForNotification(with deviceIdentifier: String,
                             token: String,
                             completion: @escaping DetailsCompletion) -> URLSessionDataTask? {
    let urlRoute = String(format: "%@cloud/", baseServiceUrl)
    let data: [String: Any] = ["device": deviceIdentifier, "cloud_id": token]

    return HTTPHandler.performPostRequest(urlRoute, data: data, completion: completion)
  }

  func paths(completion: @escaping ListCompletion) -> URLSessionDataTask? {
    let urlRoute = String(format: "%@paths/", baseServiceUrl)

    return HTTPHandler.performGetRequest(urlRoute, completion: completion)
  }
}
