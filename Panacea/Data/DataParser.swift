//
//  DataParser.swift
//  Panacea
//
//  Created by Blashadow on 6/15/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class DataParser: NSObject {
  public static func path(width rawItem: [String: Any]) -> Path {
    let identifier = rawItem["identifier"] as? Int ?? Int.random(in: Range(1..<1000000))
    let name = rawItem["name"] as? String ?? ""
    let details = rawItem["details"] as? String ?? ""
    let imageUrl = rawItem["image_url"] as? String ?? ""
    let rawPlaces = rawItem["places"] as? [[String: Any]] ?? []
    let places = rawPlaces.map({ DataParser.placeWith(rawData: $0) })

    return Path(id: identifier, name: name, details: details, imageUrl: imageUrl, places: places)
  }

  public static func placeWith(rawData rawItem: [String: Any]) -> Place {
    let item = Place()

    item.identifier = rawItem["id"] as? Int ?? 0
    item.placeTitle = rawItem["title"] as? String
    item.placeImageHeaderUrl = rawItem["image_cover"] as? String
    item.favoriteCount = rawItem["favorites"] as? Int ?? 0
    item.placePrice = rawItem["cover"] as? Float ?? 0
    item.placeDescription = rawItem["description"] as? String
    item.dueDate = parseDate((rawItem["due_date"] as? String) ?? "")
    item.mapUrl = rawItem["map_image"] as? String
    item.shareMessage = rawItem["share_message"] as? String
    item.placeType = (rawItem["entity_type"] as? String ?? "") == "Place" ? PlaceType.place : PlaceType.tour

    if let mapUrl = item.mapUrl {
      item.mapUrl = mapUrl.replacingOccurrences(of: "800x300", with: "2133x800")
    }

    if let location = rawItem["location"] as? [String: Double] {
      if let latitude = location["latitude"], let longitude = location["longitude"] {
        item.placeLocation = PlaceLocation(latitude: latitude, longitude: longitude)
      }
    }

    if let images: [[String: Any]] = rawItem["images"] as? [[String : Any]] {
      let items = images.map { (item: [String: Any]) -> PlaceImage? in
        guard let urlImage = item["url"] as? String,
          let identifier = item["id"] as? Int else {
          return nil
        }

        return PlaceImage("Image title \(identifier)", imageUrl: urlImage, identifier: identifier)
      }

      if let placeImages = (items.filter {$0 != nil} as? [PlaceImage]) {
        /// use one of theses images cause these images have better quality than the header ones
        item.placeImageHeaderUrl = placeImages.first?.imageUrl ?? item.placeImageHeaderUrl

        item.images = Array(Set(placeImages))
      }
    }

    if let targets = rawItem["targets"] as? [[String : Any]] {
      item.targets = parseTarget(targets)
    }

    if let rawWeather = rawItem["weather"] as? [String: Any] {
      item.weather = parseWeatherData(rawWeather)
    }

    /// Parse company name
    if let company = rawItem["company"] as? [String: Any] {
      let companyName = company["name"] as? String ?? ""

      item.companyPhone = company["phone"] as? String ?? ""
      item.companyName = companyName
    }

    /// Parse source event
    if let source = rawItem["source"] as? String {
      item.eventSource = source
    }

    return item
  }

  private static func parseWeatherData(_ rawData: [String: Any]) -> Weather? {
    guard let current = rawData["current"] as? Int,
      let maxTemperature = rawData["max"] as? Int,
      let minTemperature = rawData["min"] as? Int,
      let statusTemperature = rawData["text"] as? String,
      let iconUrl = rawData["icon"] as? String else {
        return nil
    }

    let item = Weather(
      temperature: current,
      maxTemperature: maxTemperature,
      minTemperature: minTemperature,
      status: statusTemperature,
      iconUrl: iconUrl
    )

    var forecastItems: [ForecastItem] = []

    if let rawForecastData = rawData["forecast"] as? [[String: Any]] {
      for item in rawForecastData {
        if let item = DataParser.parseForecastItem(rawData: item) {
          forecastItems.append(item)
        }
      }
    }

    item.forecast = forecastItems

    return item
  }

  private static func parseForecastItem(rawData: [String: Any]) -> ForecastItem? {
    guard let minTemperature = rawData["min"] as? Int,
      let maxTemperature = rawData["max"] as? Int,
      let forecastDescription = rawData["text"] as? String,
      let date = parseDate(rawData["date"] as? String ?? ""),
      let iconUrl = rawData["icon"] as? String else {
        return nil
    }
    let identifier = Date.init().timeIntervalSince1970

    return ForecastItem(identifier: identifier,
                        description: forecastDescription,
                        maxTemperature: maxTemperature,
                        minTemperature: minTemperature,
                        iconUrl: iconUrl,
                        date: date)
  }

  private static func parseTarget(_ targets: [[String: Any]]) -> [PlaceTarget] {
    let items = targets.map { (item: [String: Any]) -> PlaceTarget? in
      guard let title = item["title"] as? String,
        let id = item["id"] as? Int,
        let price = item["cover"] as? Int,
        let imageUrl = item["header_image"] as? String,
        let targetDescription = item["description"] as? String,
        let rawDate = item["due_date"] as? String else {
          return nil
      }

      let itemTarget = PlaceTarget()
      itemTarget.identifier = id
      itemTarget.placeTitle = title
      itemTarget.dueDate = Date.init()
      itemTarget.placePrice = Float(price)
      itemTarget.placeImageHeaderUrl = imageUrl
      itemTarget.dueDate = parseDate(rawDate)
      itemTarget.placeDescription = targetDescription

      return itemTarget
    }

    return items.filter {$0 != nil} as? [PlaceTarget] ?? []
  }

  private static func parseDate(_ rawDate: String) -> Date? {
    let dateFormatter = DateFormatter()

    // Example 2019-07-20T10:00:00
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.locale = Locale.current

    return dateFormatter.date(from: rawDate)
  }
}
