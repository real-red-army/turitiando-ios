//
//  RemoteNotification.swift
//  Panacea
//
//  Created by Luis Romero on 10/9/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import RealmSwift
import IGListKit

enum NotificationType {
  case place
  case subscriptionList
  case weeklyReport
  case tour
}

// PLACE
// SUBSCRIPTION_LIST
// REPORT_WEEKLY
// TOUR

class RemoteNotification: Object {
  @objc dynamic var entityId: Double = 0
  @objc dynamic var identifier: Double = 0
  @objc dynamic var title = ""
  @objc dynamic var details = ""
  @objc dynamic var notificationType = ""

  var type: NotificationType {
    if notificationType == "PLACE" {
      return NotificationType.place
    } else if notificationType == "tour" {
      return NotificationType.tour
    } else if notificationType == "REPORT_WEEKLY" {
      return NotificationType.weeklyReport
    } else if notificationType == "SUBSCRIPTION_LIST" {
      return NotificationType.subscriptionList
    }

    return NotificationType.place
  }

  convenience init(_ id: Double,
                   title: String,
                   details: String,
                   notificationType: String,
                   entityId: Double) {
    self.init()

    self.identifier = id
    self.title = title
    self.details = details
    self.notificationType = notificationType
  }

  static func createLoadingObject() -> RemoteNotification {
    let notification = RemoteNotification()

    return notification
  }
}

extension RemoteNotification: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return String(format: "\(identifier)") as NSString
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? RemoteNotification else {
      return false
    }

    return self.identifier == object.identifier
  }
}
