//
//  PlaceImage.swift
//  Panacea
//
//  Created by Blashadow on 7/6/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class PlaceImage: Equatable, Hashable {
  var id: Int = 0
  var title: String = ""
  var imageUrl: String = ""

  init(_ title: String, imageUrl: String, identifier: Int) {
    self.title = title
    self.imageUrl = imageUrl
    self.id = identifier
  }

  func hash(into hasher: inout Hasher) {
    hasher.combine(id)
    hasher.combine(title)
    hasher.combine(imageUrl)
  }

  static func == (left:PlaceImage, right:PlaceImage) -> Bool {
    return left.id == right.id
  }
}

extension PlaceImage: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return String(format: "\(title)\(imageUrl)") as NSString
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? PlaceImage else {
      return false
    }

    return self.imageUrl == object.imageUrl
  }
}
