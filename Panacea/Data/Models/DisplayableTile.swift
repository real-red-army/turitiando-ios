//
//  DisplayableTile.swift
//  Panacea
//
//  Created by Blashadow on 7/8/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

protocol DisplayableTile {
  var title: String? { get }
  var details: String? { get }
  var imageHeaderUrl: String? { get }
  var price: Float? { get }
  var identifier: Int { get }
}
