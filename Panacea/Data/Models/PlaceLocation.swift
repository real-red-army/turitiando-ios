//
//  PlaceLocation.swift
//  Panacea
//
//  Created by Blashadow on 7/14/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class PlaceLocation {
  let latitude: Double
  let longitude: Double

  init(latitude: Double, longitude: Double) {
    self.latitude = latitude
    self.longitude = longitude
  }
}
