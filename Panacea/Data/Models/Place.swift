//
//  Place.swift
//  Panacea
//
//  Created by Blashadow on 10/20/18.
//  Copyright © 2018 blashadow. All rights reserved.
//

import UIKit
import IGListKit

enum PlaceType {
  case place
  case tour
}

class Place: NSObject {
  var identifier: Int
  var placeTitle: String?
  var placeImageHeaderUrl: String?
  var favoriteCount: Int?
  var placePrice: Float?
  var placeDescription: String?
  var mapUrl: String?
  var shareMessage: String?
  var dueDate: Date?
  var placeLocation: PlaceLocation?
  var images: [PlaceImage]?
  var targets: [PlaceTarget]?
  var weather: Weather?
  var placeType: PlaceType?

  var eventSource: String = ""
  var companyName: String = ""
  var companyPhone: String = ""

  override init() {
    self.identifier = 0
  }

  init(identifier: Int,
       title: String,
       description: String,
       imageUrl: String,
       favoriteCount: Int,
       price: Float,
       images: [PlaceImage]) {
    self.identifier = identifier
    self.placeTitle = title
    self.placeImageHeaderUrl = imageUrl
    self.favoriteCount = favoriteCount
    self.placePrice = price
    self.placeDescription = description

    self.images = images
  }

  //Return a remaining string based on due date and current date '6 days remaining'
  func remainingDays() -> String {
    let current = Date()
    let endDate = dueDate ?? Date()

    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.day]
    formatter.unitsStyle = .full
    let string = formatter.string(from: current, to: endDate) ?? ""

    return "\(string) remaining"
  }

  //Convert due date to string with this format Monday, Nov 6
  func dueDateString() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "EEEE, MMM d"

    return formatter.string(from: dueDate ?? Date())
  }
}

extension Place: DisplayableTile {
  var title: String? {
    return placeTitle
  }

  var details: String? {
    return placeDescription
  }

  var imageHeaderUrl: String? {
    return placeImageHeaderUrl
  }

  var price: Float? {
    return placePrice
  }
}

extension Place: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return String(format: "\(identifier)") as NSString
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? Place else {
      return false
    }

    return self.identifier == object.identifier
  }
}
