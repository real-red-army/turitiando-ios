//
//  Path.swift
//  Panacea
//
//  Created by Luis Romero on 10/13/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class Path: NSObject {
  let identifier: Int
  let name: String
  let pathDetails: String
  let imageUrl: String
  let places: [Place]

  init(id identifier: Int, name: String, details: String, imageUrl: String, places: [Place]) {
    self.identifier = identifier
    self.name = name
    self.pathDetails = details
    self.places = places
    self.imageUrl = imageUrl
  }
}

extension Path: DisplayableTile {
  var details: String? {
    return pathDetails
  }

  var title: String? {
    return name
  }

  var imageHeaderUrl: String? {
    return imageUrl
  }

  var price: Float? {
    return nil
  }
}

extension Path: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return String(format: "\(identifier)") as NSString
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? Path else {
      return false
    }

    return self.identifier == object.identifier
  }
}
