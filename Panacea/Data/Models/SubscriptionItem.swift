//
//  SubscriptionItem.swift
//  Panacea
//
//  Created by Luis Romero on 3/15/20.
//  Copyright © 2020 blashadow. All rights reserved.
//

import RealmSwift
import IGListKit

class SubscriptionItem: Object {
  @objc dynamic var identifier: String = ""
  @objc dynamic var placeId: Double = 0

  convenience init(_ id: String, placeId: Double) {
    self.init()

    self.identifier = id
    self.placeId = placeId
  }
}

extension SubscriptionItem: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return String(format: "\(identifier)") as NSString
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? SubscriptionItem else {
      return false
    }

    return self.identifier == object.identifier
  }
}
