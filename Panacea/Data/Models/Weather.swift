//
//  Weather.swift
//  Panacea
//
//  Created by Blashadow on 7/14/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class Weather {
  var currentTemperature: Int
  var minTemperature: Int
  var maxTemperature: Int
  var status: String
  var iconUrl: String
  var forecast: [ForecastItem]?

  init(temperature: Int,
       maxTemperature: Int,
       minTemperature: Int,
       status: String,
       iconUrl: String) {
    self.currentTemperature = temperature
    self.minTemperature = minTemperature
    self.maxTemperature = maxTemperature
    self.status = status
    self.iconUrl = iconUrl
  }
}
