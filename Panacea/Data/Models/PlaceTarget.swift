//
//  PlaceTarget.swift
//  Panacea
//
//  Created by Blashadow on 7/7/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import IGListKit

class PlaceTarget: NSObject {
  var identifier: Int = 0
  var placeTitle: String?
  var placeDescription: String?
  var placePrice: Float?
  var placeImageHeaderUrl: String?
  var dueDate: Date?
}

extension PlaceTarget: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return String(format: "\(identifier)") as NSString
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? PlaceTarget else {
      return false
    }
    return self.identifier == object.identifier
  }
}

extension PlaceTarget: DisplayableTile {
  var title: String? {
    return placeTitle
  }

  var details: String? {
    return placeDescription
  }

  var imageHeaderUrl: String? {
    return placeImageHeaderUrl
  }

  var price: Float? {
    return placePrice
  }
}
