//
//  ForecastItem.swift
//  Panacea
//
//  Created by Luis Romero on 11/2/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import IGListKit

class ForecastItem: NSObject {
  let identifier: Double
  let maxTemperature: Int
  let minTemperature: Int
  let forecastDescription: String
  let iconUrl: String
  let forecastDate: Date

  init(identifier: Double,
       description: String,
       maxTemperature: Int,
       minTemperature: Int,
       iconUrl: String,
       date: Date) {
    self.identifier = identifier
    self.forecastDescription = description
    self.maxTemperature = maxTemperature
    self.minTemperature = minTemperature
    self.iconUrl = iconUrl
    self.forecastDate = date
  }
}

extension ForecastItem: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return String(format: "\(identifier)") as NSString
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? ForecastItem else {
      return false
    }

    return self.identifier.isEqual(to: object.identifier)
  }
}
