//
//  ForecastCollectionViewCell.swift
//  Panacea
//
//  Created by Luis Romero on 11/4/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke

class ForecastCollectionViewCell: UICollectionViewCell {
  let container: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  let weatherImageView: UIImageView = {
    let imageView = UIImageView(frame: .zero)
    imageView.translatesAutoresizingMaskIntoConstraints = false

    return imageView
  }()

  let statusContainer: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  let statusWeatherLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont(name: FontNames.Regular, size: 14)
    label.textColor = UIColor(hexValue: Colors.Text)
    label.numberOfLines = 0

    return label
  }()

  let dateWeatherLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont(name: FontNames.Regular, size: 12)
    label.textColor = UIColor(hexValue: 0x5A6878)
    label.numberOfLines = 0

    return label
  }()

  let maxTemperatureLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont(name: FontNames.Regular, size: 13)
    label.textColor = UIColor(hexValue: Colors.Title)

    return label
  }()

  let minTemperatureLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont(name: FontNames.Regular, size: 12)
    label.textColor = UIColor(hexValue: 0x5A6878)

    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupCell()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setupCell(with forecastItem: ForecastItem) {
    guard let imageUrl = URL(string: forecastItem.iconUrl) else {
      return
    }

    let options = ImageLoadingOptions(
        placeholder: UIImage(named: "placeholder"),
        transition: .fadeIn(duration: 0.33)
    )

    Nuke.loadImage(with: imageUrl, into: weatherImageView)
    Nuke.loadImage(with: imageUrl, options: options, into: weatherImageView, progress: nil) { (response, _) in
      self.weatherImageView.image = response?.image
      self.weatherImageView.image = self.weatherImageView.image?.withRenderingMode(.alwaysTemplate)
      self.weatherImageView.tintColor = UIColor(hexValue: Colors.Title)
    }

    minTemperatureLabel.text = "\(forecastItem.minTemperature) ℃"
    maxTemperatureLabel.text = "\(forecastItem.maxTemperature) ℃"
    statusWeatherLabel.text = forecastItem.forecastDescription
    dateWeatherLabel.text = forecastItem.forecastDate.weekDayStr
  }

  private func setupCell() {
    translatesAutoresizingMaskIntoConstraints = false
    addSubview(container)

    NSLayoutConstraint.activate([
      container.widthAnchor.constraint(equalTo: widthAnchor, constant: -10),
      container.heightAnchor.constraint(equalTo: heightAnchor, constant: -10),
      container.centerYAnchor.constraint(equalTo: centerYAnchor),
      container.centerXAnchor.constraint(equalTo: centerXAnchor)
    ])

    container.addSubview(weatherImageView)
    NSLayoutConstraint.activate([
      weatherImageView.widthAnchor.constraint(equalToConstant: 50),
      weatherImageView.heightAnchor.constraint(equalToConstant: 50),
      weatherImageView.centerYAnchor.constraint(equalTo: container.centerYAnchor),
      weatherImageView.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 10)
    ])

    container.addSubview(minTemperatureLabel)
    NSLayoutConstraint.activate([
      minTemperatureLabel.widthAnchor.constraint(equalToConstant: 40),
      minTemperatureLabel.heightAnchor.constraint(equalToConstant: 30),
      minTemperatureLabel.centerYAnchor.constraint(equalTo: container.centerYAnchor),
      minTemperatureLabel.rightAnchor.constraint(equalTo: container.rightAnchor)
    ])

    container.addSubview(maxTemperatureLabel)
    NSLayoutConstraint.activate([
      maxTemperatureLabel.widthAnchor.constraint(equalToConstant: 40),
      maxTemperatureLabel.heightAnchor.constraint(equalToConstant: 30),
      maxTemperatureLabel.centerYAnchor.constraint(equalTo: container.centerYAnchor),
      maxTemperatureLabel.rightAnchor.constraint(equalTo: minTemperatureLabel.leftAnchor)
    ])

    container.addSubview(statusContainer)
    NSLayoutConstraint.activate([
      statusContainer.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      statusContainer.leftAnchor.constraint(equalTo: weatherImageView.rightAnchor, constant: 10),
      statusContainer.centerYAnchor.constraint(equalTo: container.centerYAnchor),
      statusContainer.rightAnchor.constraint(equalTo: maxTemperatureLabel.leftAnchor, constant: -10)
    ])

    statusContainer.addSubview(statusWeatherLabel)
    NSLayoutConstraint.activate([
      statusWeatherLabel.topAnchor.constraint(equalTo: statusContainer.topAnchor),
      statusWeatherLabel.widthAnchor.constraint(equalTo: statusContainer.widthAnchor),
      statusWeatherLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      statusWeatherLabel.leftAnchor.constraint(equalTo: statusContainer.leftAnchor),
      statusWeatherLabel.rightAnchor.constraint(equalTo: statusContainer.rightAnchor)
    ])

    statusContainer.addSubview(dateWeatherLabel)
    NSLayoutConstraint.activate([
      dateWeatherLabel.topAnchor.constraint(equalTo: statusWeatherLabel.bottomAnchor),
      dateWeatherLabel.widthAnchor.constraint(equalTo: statusContainer.widthAnchor),
      dateWeatherLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
      dateWeatherLabel.leftAnchor.constraint(equalTo: statusContainer.leftAnchor),
      dateWeatherLabel.rightAnchor.constraint(equalTo: statusContainer.rightAnchor),
      dateWeatherLabel.bottomAnchor.constraint(equalTo: statusContainer.bottomAnchor)
    ])
  }
}
