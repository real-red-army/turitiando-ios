//
//  CardSkeletonCollectionCell.swift
//  Panacea
//
//  Created by Luis Romero on 10/27/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class CardSkeletonCollectionCell: TitleDetailsSkeletonCell {
  override func setupConstraints() {
    contentView.addSubview(imagePlaceholderView)
    imagePlaceholderView.layer.cornerRadius = 10
    imagePlaceholderView.clipsToBounds = true

    NSLayoutConstraint.activate([
      imagePlaceholderView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
      imagePlaceholderView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
      imagePlaceholderView.heightAnchor.constraint(equalToConstant: 100),
      imagePlaceholderView.topAnchor.constraint(equalTo: contentView.topAnchor)
    ])

    contentView.addSubview(titlePlaceholderView)

    NSLayoutConstraint.activate([
      titlePlaceholderView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
      titlePlaceholderView.widthAnchor.constraint(equalTo: imagePlaceholderView.widthAnchor),
      titlePlaceholderView.heightAnchor.constraint(equalToConstant: 20),
      titlePlaceholderView.topAnchor.constraint(equalTo: imagePlaceholderView.bottomAnchor, constant: 5)
    ])

    contentView.addSubview(subtitlePlaceholderView)

    NSLayoutConstraint.activate([
      subtitlePlaceholderView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.6),
      subtitlePlaceholderView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
      subtitlePlaceholderView.heightAnchor.constraint(equalToConstant: 10),
      subtitlePlaceholderView.topAnchor.constraint(equalTo: titlePlaceholderView.bottomAnchor, constant: 5)
    ])
  }
}
