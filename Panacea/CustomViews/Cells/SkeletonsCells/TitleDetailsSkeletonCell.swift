//
//  TitleDetailsSkeletonCell.swift
//  Panacea
//
//  Created by Luis Romero on 10/27/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Skeleton

class TitleDetailsSkeletonCell: UICollectionViewCell {
  var imagePlaceholderView: GradientContainerView = {
    let view = GradientContainerView(frame: .zero)
    view.clipsToBounds = true
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var titlePlaceholderView: GradientContainerView = {
    let view = GradientContainerView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var subtitlePlaceholderView: GradientContainerView = {
    let view = GradientContainerView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

//    contentView.translatesAutoresizingMaskIntoConstraints = false

    setupConstraints()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setupConstraints() {
    contentView.addSubview(imagePlaceholderView)

    NSLayoutConstraint.activate([
      imagePlaceholderView.widthAnchor.constraint(equalToConstant: 50),
      imagePlaceholderView.heightAnchor.constraint(equalToConstant: 50),
      imagePlaceholderView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
      imagePlaceholderView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10)
    ])

    contentView.addSubview(titlePlaceholderView)

    NSLayoutConstraint.activate([
      titlePlaceholderView.leftAnchor.constraint(equalTo: imagePlaceholderView.rightAnchor, constant: 10),
      titlePlaceholderView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
      titlePlaceholderView.heightAnchor.constraint(equalToConstant: 20),
      titlePlaceholderView.topAnchor.constraint(equalTo: imagePlaceholderView.topAnchor)
    ])

    contentView.addSubview(subtitlePlaceholderView)

    NSLayoutConstraint.activate([
      subtitlePlaceholderView.leftAnchor.constraint(equalTo: imagePlaceholderView.rightAnchor, constant: 10),
      subtitlePlaceholderView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
      subtitlePlaceholderView.bottomAnchor.constraint(equalTo: imagePlaceholderView.bottomAnchor, constant: 0),
      subtitlePlaceholderView.topAnchor.constraint(equalTo: titlePlaceholderView.bottomAnchor, constant: 5)
    ])
  }
}

extension TitleDetailsSkeletonCell: GradientsOwner {
  var gradientLayers: [CAGradientLayer] {
    return [imagePlaceholderView.gradientLayer,
            titlePlaceholderView.gradientLayer,
            subtitlePlaceholderView.gradientLayer]
  }
}
