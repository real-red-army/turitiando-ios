//
//  TileImageViewCell.swift
//  Panacea
//
//  Created by Blashadow on 7/6/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke

class TileImageViewCell: UICollectionViewCell {
  let imageContainer = UIView(frame: .zero)

  let image: UIImageView = {
    let image = UIImageView(frame: .zero)

    image.translatesAutoresizingMaskIntoConstraints = false
    image.contentMode = .scaleAspectFill
    image.clipsToBounds = true

    return image
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupView()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setupImage(_ urlString: String) {
    guard let urlImage = URL(string: urlString) else {
      return
    }

    let options = ImageLoadingOptions(
        placeholder: UIImage(named: "placeholder"),
        transition: .fadeIn(duration: 0.33)
    )

    let size = 90 * UIScreen.main.scale

    let imageRequest = ImageRequest(url: urlImage,
                                    targetSize: CGSize(width: size, height: size),
                                    contentMode: .aspectFit)

    Nuke.loadImage(with: imageRequest, options: options, into: image, progress: nil) { [weak self] (imageResponse, _) in
      if let image = imageResponse?.image, let imageView = self?.image {
        imageView.image = image
      }
    }
  }

  private func setupView() {
    imageContainer.translatesAutoresizingMaskIntoConstraints = false
    imageContainer.clipsToBounds = true
    imageContainer.layer.cornerRadius = 10.0
    imageContainer.backgroundColor = UIColor.white

    contentView.addSubview(imageContainer)

    NSLayoutConstraint.activate([
      imageContainer.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -10),
      imageContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, constant: -10),
      imageContainer.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
      imageContainer.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
    ])

    imageContainer.addSubview(image)
    NSLayoutConstraint.activate([
      image.widthAnchor.constraint(equalTo: imageContainer.widthAnchor),
      image.heightAnchor.constraint(equalTo: imageContainer.heightAnchor),
      image.centerXAnchor.constraint(equalTo: imageContainer.centerXAnchor),
      image.centerYAnchor.constraint(equalTo: imageContainer.centerYAnchor)
    ])

    contentView.backgroundColor = UIColor.white
  }
}
