//
//  TileTourViewCell.swift
//  Panacea
//
//  Created by Blashadow on 6/20/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke

class TileTourViewCell: UICollectionViewCell {
  let tileImage: UIImageView = {
    let image = UIImageView(frame: .zero)
    image.translatesAutoresizingMaskIntoConstraints = false
    image.contentMode = UIViewContentMode.scaleAspectFill
    image.clipsToBounds = true

    return image
  }()

  let tileTitle: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 14)
    label.textColor = UIColor(hexValue: 0x333)
    label.backgroundColor = UIColor(hexValue: Colors.Gray)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.clipsToBounds = true
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  let tileDescription: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Light, size: 10)
    label.textColor = UIColor(hexValue: 0x333)
    label.backgroundColor = UIColor(hexValue: Colors.Gray)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.clipsToBounds = true
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  let tilePrice: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 13)
    label.textColor = UIColor(hexValue: 0x333)
    label.backgroundColor = UIColor(hexValue: Colors.Gray)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  let containerView: UIView = {
    let view = UIView(frame: .zero)

    view.translatesAutoresizingMaskIntoConstraints = false
    view.clipsToBounds = true
    view.backgroundColor = UIColor(hexValue: Colors.Gray)

    return view
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupView()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setupDataCell(title: String, description: String, imageUrl: String, price: Int) {
    tileTitle.text = title.capitalized
    tileDescription.text = description
    tilePrice.text = "$\(price)"

    containerView.layer.cornerRadius = 10.0
    containerView.layer.masksToBounds = true

    if price < 1 {
      tilePrice.isHidden = true
    }

    guard let imageURL = URL(string: imageUrl) else {
      return
    }

    let size = 100 * UIScreen.main.scale
    let imageRequest = ImageRequest(url: imageURL,
                                    targetSize: CGSize(width: size, height: size),
                                    contentMode: .aspectFit)

    let options = ImageLoadingOptions(
      placeholder: UIImage(named: "placeholder"),
      transition: .fadeIn(duration: 0.33)
    )

    Nuke.loadImage(with: imageRequest,
                   options: options,
                   into: tileImage,
                   progress: nil) { [weak self ](imageResponse, _) in
      if let imageView = self?.tileImage, let image = imageResponse?.image {
        imageView.image = image
      }
    }
  }

  private func setupView() {
    containerView.backgroundColor = UIColor(hexValue: Colors.Gray)
    containerView.clipsToBounds = true
    containerView.isOpaque = true
    containerView.layer.backgroundColor = UIColor(hexValue: Colors.Gray).cgColor

    contentView.backgroundColor = UIColor.white
    contentView.addSubview(containerView)

    containerView.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -10).isActive = true
    containerView.heightAnchor.constraint(equalTo: contentView.heightAnchor, constant: -10).isActive = true
    containerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    containerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true

    containerView.addSubview(tileImage)

    tileImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
    tileImage.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
    tileImage.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
    tileImage.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true

    containerView.addSubview(tileTitle)

    NSLayoutConstraint.activate([
      tileTitle.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
      tileTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: 20),
      tileTitle.leadingAnchor.constraint(equalTo: tileImage.trailingAnchor, constant: 10),
      tileTitle.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 1.0)
    ])

    containerView.addSubview(tileDescription)

    tileDescription.topAnchor.constraint(equalTo: tileTitle.bottomAnchor, constant: 1).isActive = true
    tileDescription.leadingAnchor.constraint(equalTo: tileImage.trailingAnchor, constant: 10).isActive = true
    tileDescription.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0).isActive = true
    tileDescription.heightAnchor.constraint(greaterThanOrEqualToConstant: 40).isActive = true

    containerView.addSubview(tilePrice)

    NSLayoutConstraint.activate([
      tilePrice.topAnchor.constraint(equalTo: tileDescription.bottomAnchor, constant: 1),
      tilePrice.leadingAnchor.constraint(equalTo: tileImage.trailingAnchor, constant: 10),
      tilePrice.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 1.0),
      tilePrice.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -10)
    ])
  }
}
