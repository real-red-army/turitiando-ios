//
//  TileCollectionViewCell.swift
//  Panacea
//
//  Created by Blashadow on 6/16/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke

class TilePlaceViewCell: UICollectionViewCell {
  let tileTitle: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 11)
    label.textColor = UIColor.white
    label.textAlignment = .center
    label.numberOfLines = 0
    label.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  let tileImage: UIImageView = {
    let image = UIImageView(frame: .zero)
    image.translatesAutoresizingMaskIntoConstraints = false
    image.contentMode = .scaleAspectFill

    return image
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupCell()
  }

  func setupDataCell(title: String, imageUrl: String) {
    self.tileTitle.text = title

    guard let imageURL = URL(string: imageUrl) else {
      return
    }

    let size = 100 * UIScreen.main.scale
    let imageRequest = ImageRequest(url: imageURL,
                                    targetSize: CGSize(width: size, height: size),
                                    contentMode: .aspectFit)

    let options = ImageLoadingOptions(
      placeholder: UIImage(named: "placeholder"),
      transition: .fadeIn(duration: 0.33)
    )

    Nuke.loadImage(with: imageRequest,
                   options: options,
                   into: tileImage,
                   progress: nil) { [weak self ](imageResponse, _) in
      if let imageView = self?.tileImage, let image = imageResponse?.image {
        imageView.image = image
      }
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupCell() {
    contentView.backgroundColor = UIColor.white
    contentView.addSubview(tileImage)

    tileImage.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 1.0).isActive = true
    tileImage.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 1.0).isActive = true
    tileImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
    tileImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true

    contentView.addSubview(tileTitle)

    tileTitle.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.7).isActive = true
    tileTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
    tileTitle.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
    tileTitle.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true

    contentView.layer.cornerRadius = 10.0
    contentView.clipsToBounds = true
  }
}
