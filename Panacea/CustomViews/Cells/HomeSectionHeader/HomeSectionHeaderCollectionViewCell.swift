//
//  HomeSectionHeaderCollectionViewCell.swift
//  Panacea
//
//  Created by Blashadow on 6/16/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class HomeSectionHeaderCollectionViewCell: UICollectionViewCell {
  var tapViewAllHandler: (() -> Void)?
  var shouldShowViewAllLabel: Bool = true {
    didSet {
      showAllLabel.isHidden = !shouldShowViewAllLabel
    }
  }

  let label: UILabel = {
    let item = UILabel(frame: .zero)

    item.translatesAutoresizingMaskIntoConstraints = false
    item.font = UIFont(name: FontNames.Regular, size: 20)
    item.textColor = UIColor.init(hexValue: Colors.Text)
    item.backgroundColor = UIColor.white
    item.numberOfLines = 0
    item.textAlignment = .left

    return item
  }()

  let showAllLabel: UILabel = {
    let item = UILabel(frame: .zero)

    item.translatesAutoresizingMaskIntoConstraints = false
    item.font = UIFont(name: FontNames.Medium, size: 15)
    item.textColor = UIColor.init(hexValue: Colors.Text)
    item.backgroundColor = UIColor.white
    item.numberOfLines = 0
    item.textAlignment = .left

    let underlineAttribute = [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
    let underlineAttributedString = NSAttributedString(string: "Mostrar todos", attributes: underlineAttribute)
    item.attributedText = underlineAttributedString

    return item
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupCell()
  }

  @objc private func didTapLabel() {
    if let handler = tapViewAllHandler {
      handler()
    }
  }

  private func setupCell() {
    contentView.addSubview(label)

    NSLayoutConstraint.activate([
      label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
      label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      label.widthAnchor.constraint(equalTo: contentView.widthAnchor),
      label.heightAnchor.constraint(equalTo: contentView.heightAnchor)
    ])

    contentView.addSubview(showAllLabel)
    showAllLabel.isUserInteractionEnabled = true

    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapLabel))
    showAllLabel.addGestureRecognizer(tapGesture)

    NSLayoutConstraint.activate([
      showAllLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
      showAllLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      showAllLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 10),
      showAllLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 10)
    ])
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
