//
//  NotificationCollectionViewCell.swift
//  Panacea
//
//  Created by Luis Romero on 10/9/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class NotificationCollectionViewCell: UICollectionViewCell {
  static let marginHorizontal: CGFloat = 8
  static let containerMarginHorizontal: CGFloat = 20
  static let iconSize: CGFloat = 30

  let containerView: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  let notificationIcon: UIImageView = {
    let imageView = UIImageView(frame: .zero)
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = UIViewContentMode.scaleAspectFill
    imageView.clipsToBounds = true

    return imageView
  }()

  let title: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 18)
    label.textColor = UIColor(hexValue: Colors.Title)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  let details: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 12)
    label.textColor = UIColor(hexValue: 0x8C8997)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  let subTitle: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Light, size: 10)
    label.textColor = UIColor(hexValue: 0x8C8997)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupCell()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setupDataCell(with notification: RemoteNotification) {
    self.title.text = notification.title.capitalizingFirstLetter()
    self.details.text = notification.details.capitalizingFirstLetter()
    self.subTitle.text = notification.title.capitalizingFirstLetter()

    var imageName: String = NotificationImage.Default

    if notification.notificationType == "TOUR" {
      imageName = NotificationImage.Tour
    } else if notification.notificationType == "PLACE" {
      imageName = NotificationImage.Place
    } else if notification.notificationType == "WEEKLY" {
      imageName = NotificationImage.Weekly
    }

    notificationIcon.image = UIImage(named: imageName)
  }

  private func setupCell() {
    contentView.addSubview(containerView)
    let marginHorizontal: CGFloat = NotificationCollectionViewCell.marginHorizontal
    let containerMarginHorizontal: CGFloat = NotificationCollectionViewCell.containerMarginHorizontal

    containerView.layer.masksToBounds = false
    containerView.backgroundColor = UIColor.white
    containerView.layer.shadowRadius = 2
    containerView.layer.shadowColor = UIColor(hexValue: 0x333).cgColor
    containerView.layer.shadowOpacity = 0.2
    containerView.layer.shadowOffset = CGSize(width: 0, height: 1.0)

    NSLayoutConstraint.activate([
      containerView.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -(containerMarginHorizontal)),
      containerView.heightAnchor.constraint(equalTo: contentView.heightAnchor, constant: -(containerMarginHorizontal)),
      containerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      containerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
    ])

    containerView.addSubview(notificationIcon)

    NSLayoutConstraint.activate([
      notificationIcon.widthAnchor.constraint(equalToConstant: NotificationCollectionViewCell.iconSize),
      notificationIcon.heightAnchor.constraint(equalToConstant: NotificationCollectionViewCell.iconSize),
      notificationIcon.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
      notificationIcon.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: marginHorizontal)
    ])

    containerView.addSubview(title)

    NSLayoutConstraint.activate([
      title.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      title.leftAnchor.constraint(equalTo: notificationIcon.rightAnchor, constant: marginHorizontal),
      title.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: marginHorizontal),
      title.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 4)
    ])

    containerView.addSubview(subTitle)

    NSLayoutConstraint.activate([
      subTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      subTitle.leftAnchor.constraint(equalTo: notificationIcon.rightAnchor, constant: marginHorizontal),
      subTitle.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: marginHorizontal),
      subTitle.topAnchor.constraint(equalTo: title.bottomAnchor)
    ])

    containerView.addSubview(details)

    NSLayoutConstraint.activate([
      details.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      details.topAnchor.constraint(equalTo: subTitle.bottomAnchor),
      details.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: marginHorizontal),
      details.leftAnchor.constraint(equalTo: notificationIcon.rightAnchor, constant: marginHorizontal)
    ])
  }
}
