//
//  CollectionViewHorizontalCell.swift
//  Panacea
//
//  Created by Blashadow on 6/16/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class CollectionViewHorizontalCell: UICollectionViewCell {
  let collectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal

    let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = UIColor.white
    view.layoutMargins = .zero
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false

    return view
  }()

  private var sectionItems: SectionItems?

  lazy var adapter: ListAdapter = {
    let updater = ListAdapterUpdater()
    return ListAdapter(updater: updater, viewController: self.inputViewController, workingRangeSize: 0)
  }()

  private let animateCollectionView = AnimatedCollectionView()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupCell()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupCell() {
    contentView.addSubview(collectionView)

    collectionView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 1.0).isActive = true
    collectionView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 1.0).isActive = true
  }

  public func setupCell(width sectionItem: SectionItems) {
    self.sectionItems = sectionItem

    self.adapter.collectionView = self.collectionView
    self.adapter.dataSource = self
    self.adapter.collectionViewDelegate = animateCollectionView
  }
}

extension CollectionViewHorizontalCell: ListAdapterDataSource {
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    return self.sectionItems?.itemType.value as? [ListDiffable] ?? []
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    if case .tourTile(_) = self.sectionItems?.itemType {
      return TourTileController(with: self.sectionItems?.itemPressHandler, style: .customWidth(300))
    }

    return TileController(with: self.sectionItems?.itemPressHandler)
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}
