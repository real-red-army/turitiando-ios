//
//  TourCollectionViewCell.swift
//  Panacea
//
//  Created by Blashadow on 6/22/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import Nuke

class TourCollectionViewCell: UICollectionViewCell {
  static let tourDateHeight = 120

  let containerView: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  let tourImage: UIImageView = {
    let imageView = UIImageView(frame: .zero)

    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
    imageView.backgroundColor = UIColor.white
    imageView.translatesAutoresizingMaskIntoConstraints = false

    return imageView
  }()

  let tourTitle: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Medium, size: 18)
    label.textColor = UIColor(hexValue: 0x555)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.clipsToBounds = true
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  let tourPrice: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Regular, size: 13)
    label.textColor = UIColor(hexValue: 0x555)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  let tourDate: UILabel = {
    let label = UILabel(frame: .zero)
    label.font = UIFont(name: FontNames.Light, size: 11)
    label.textColor = UIColor(hexValue: 0x555)
    label.textAlignment = .left
    label.numberOfLines = 0
    label.backgroundColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false

    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupCell()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setupDataCell(title: String, description: String, imageUrl: String, price: Int, date: String) {
    tourTitle.text = title
    tourPrice.text = String(format: "$%d", price)
    tourDate.text = date

    guard let urlImage = URL(string: imageUrl) else {
      return
    }

    let size = 180 * UIScreen.main.scale
    let imageRequest = ImageRequest(url: urlImage,
                                    targetSize: CGSize(width: size, height: size),
                                    contentMode: .aspectFit)

    let options = ImageLoadingOptions(
      placeholder: UIImage(named: "placeholder"),
      transition: .fadeIn(duration: 0.33)
    )

    Nuke.loadImage(with: imageRequest,
                   options: options,
                   into: tourImage,
                   progress: nil) { [weak self ](imageResponse, _) in
      if let imageView = self?.tourImage, let image = imageResponse?.image {
        imageView.image = image
        imageView.layer.cornerRadius = 10.0
      }
    }
  }

  private func setupCell() {
    contentView.addSubview(containerView)

    NSLayoutConstraint.activate([
      containerView.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -10),
      containerView.heightAnchor.constraint(equalTo: contentView.heightAnchor, constant: -10),
      containerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      containerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
    ])

    containerView.addSubview(tourImage)

    NSLayoutConstraint.activate([
      tourImage.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1.0, constant: -10),
      tourImage.heightAnchor.constraint(equalToConstant: 180),
      tourImage.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
      tourImage.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
    ])

    containerView.addSubview(tourTitle)

    let constant = CGFloat(-(TourCollectionViewCell.tourDateHeight))

    NSLayoutConstraint.activate([
      tourTitle.widthAnchor.constraint(equalTo: tourImage.widthAnchor, constant: constant),
      tourTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      tourTitle.leftAnchor.constraint(equalTo: tourImage.leftAnchor),
      tourTitle.topAnchor.constraint(equalTo: tourImage.bottomAnchor, constant: 4)
    ])

    containerView.addSubview(tourPrice)

    NSLayoutConstraint.activate([
      tourPrice.widthAnchor.constraint(equalToConstant: 100),
      tourPrice.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      tourPrice.topAnchor.constraint(equalTo: tourTitle.bottomAnchor),
      tourPrice.leadingAnchor.constraint(equalTo: tourTitle.leadingAnchor)
    ])

    containerView.addSubview(tourDate)

    NSLayoutConstraint.activate([
      tourDate.widthAnchor.constraint(equalToConstant: CGFloat(TourCollectionViewCell.tourDateHeight)),
      tourDate.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      tourDate.topAnchor.constraint(equalTo: tourImage.bottomAnchor, constant: 10),
      tourDate.trailingAnchor.constraint(equalTo: tourImage.trailingAnchor)
    ])

    contentView.layer.cornerRadius = 10.0
    contentView.clipsToBounds = true
  }
}
