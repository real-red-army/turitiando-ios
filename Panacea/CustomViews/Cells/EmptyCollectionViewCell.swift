//
//  EmptyCollectionViewCell.swift
//  Panacea
//
//  Created by Luis Romero on 10/26/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class EmptyCollectionViewCell: UIView {
  var container: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var emptyIcon: UIImageView = {
    let view = UIImageView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }()

  var title: UILabel = {
    let label = UILabel(frame: .zero)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont(name: FontNames.SemiBold, size: 16)
    label.backgroundColor = UIColor.white
    label.textAlignment = .center
    label.numberOfLines = 0
    label.textColor = UIColor(hexValue: 0xA0A9B8)

    return label
  }()

  var details: UILabel = {
    let label = UILabel(frame: .zero)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont(name: FontNames.Regular, size: 14)
    label.backgroundColor = UIColor.white
    label.textAlignment = .center
    label.numberOfLines = 0
    label.textColor = UIColor(hexValue: 0xA0A9B8)

    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupCell()
  }

  func setupDataCell() {
    title.text = "Sin notificaciones aun!"
    details.text = "Te notificaremos una vez tengamos algo para ti!"
    emptyIcon.image = UIImage(named: "EmptyNotifications")
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupCell() {
    addSubview(container)

    NSLayoutConstraint.activate([
      container.widthAnchor.constraint(equalTo: widthAnchor),
      container.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      container.centerYAnchor.constraint(equalTo: centerYAnchor),
      container.centerXAnchor.constraint(equalTo: centerXAnchor)
    ])

    container.addSubview(emptyIcon)

    NSLayoutConstraint.activate([
      emptyIcon.widthAnchor.constraint(equalToConstant: 100),
      emptyIcon.heightAnchor.constraint(equalToConstant: 100),
      emptyIcon.centerXAnchor.constraint(equalTo: container.centerXAnchor),
      emptyIcon.topAnchor.constraint(equalTo: container.topAnchor)
    ])

    container.addSubview(title)

    NSLayoutConstraint.activate([
      title.topAnchor.constraint(equalTo: emptyIcon.bottomAnchor, constant: 10),
      title.centerXAnchor.constraint(equalTo: container.centerXAnchor),
      title.widthAnchor.constraint(equalTo: container.widthAnchor),
      title.heightAnchor.constraint(greaterThanOrEqualToConstant: 10)
    ])

    container.addSubview(details)

    NSLayoutConstraint.activate([
      details.topAnchor.constraint(equalTo: title.bottomAnchor),
      details.centerXAnchor.constraint(equalTo: container.centerXAnchor),
      details.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.9),
      details.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),
      details.bottomAnchor.constraint(equalTo: container.bottomAnchor)
    ])
  }
}
