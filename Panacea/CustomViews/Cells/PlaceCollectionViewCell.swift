//
//  PlaceCollectionViewCell.swift
//  Panacea
//
//  Created by Blashadow on 10/20/18.
//  Copyright © 2018 blashadow. All rights reserved.
//

import UIKit
import Nuke

class PlaceCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var placeHeaderImage: UIImageView!
  @IBOutlet weak var placeName: UILabel! {
    didSet {
      placeName.textColor = .white
    }
  }

  @IBOutlet weak var favoriteCount: UILabel! {
    didSet {
      favoriteCount.textColor = .white
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  func setupCellWithPlace(place: Place?) {
    placeName.text = place?.placeTitle ?? ""
    favoriteCount.text = "\(place?.favoriteCount ?? 0)"

    if let imageUrl = URL(string: place?.placeImageHeaderUrl ?? "") {
      Nuke.loadImage(with: imageUrl, into: placeHeaderImage)
    }
  }
}
