//
//  CircleCloseButton.swift
//  Panacea
//
//  Created by Luis Romero on 10/25/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

typealias TapHandler = () -> Void

class CircleCloseButton: UIView {
  var icon: UIView = {
    let imageview = UIImageView(image: UIImage(named: "CrossIcon"))
    imageview.translatesAutoresizingMaskIntoConstraints = false
    imageview.clipsToBounds = true
    imageview.isUserInteractionEnabled = true

    return imageview
  }()

  var tapHandler: TapHandler?

  override init(frame: CGRect) {
    super.init(frame: frame)

    self.setupView()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupView() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor(hexValue: 0x757575).withAlphaComponent(0.8)
    layer.cornerRadius = 40 / 2
    clipsToBounds = true
    isOpaque = true
    isUserInteractionEnabled = true
    addSubview(icon)

    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(localTapHandler))
    addGestureRecognizer(tapGesture)

    NSLayoutConstraint.activate([
      icon.widthAnchor.constraint(equalToConstant: 20),
      icon.heightAnchor.constraint(equalToConstant: 20),
      icon.centerYAnchor.constraint(equalTo: centerYAnchor),
      icon.centerXAnchor.constraint(equalTo: centerXAnchor)
    ])

    NSLayoutConstraint.activate([
      widthAnchor.constraint(equalToConstant: 40),
      heightAnchor.constraint(equalToConstant: 40)
    ])
  }

  @objc private func localTapHandler() {
    UIView.animate(withDuration: 0.2, animations: {
      self.transform = CGAffineTransform(rotationAngle: 180)
    }, completion: { _ in
      if let handler = self.tapHandler {
        handler()
      }
    })
  }
}
