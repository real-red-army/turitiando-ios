//
//  AnimatedCollectionViewDelegate.swift
//  Panacea
//
//  Created by Blashadow on 6/23/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class AnimatedCollectionView: NSObject {
}

extension AnimatedCollectionView: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
    UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
      if let cell = collectionView.cellForItem(at: indexPath) {
        cell.contentView.transform = .init(scaleX: 0.95, y: 0.95)
      }
    })
  }

  func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
    UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
      if let cell = collectionView.cellForItem(at: indexPath) {
        cell.contentView.transform = CGAffineTransform.identity
      }
    })
  }
}
