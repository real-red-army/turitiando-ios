//
//  TextButton.swift
//  Panacea
//
//  Created by Luis Romero on 2/5/20.
//  Copyright © 2020 blashadow. All rights reserved.
//

import UIKit

class TextButton: UIView {
  var container: UIView = {
    let view: UIView = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = UIColor.init(hexValue: Colors.ActionColor)
    view.clipsToBounds = true

    return view
  }()

  var label: UILabel = {
    let label: UILabel = UILabel(frame: .zero)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = 0
    label.textAlignment = .center
    label.text = "Reservar"
    label.font = UIFont(name: FontNames.Regular, size: 18)
    label.textColor = UIColor.white

    return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    translatesAutoresizingMaskIntoConstraints = false
    isUserInteractionEnabled = true

    setupViews()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupViews() {
    addSubview(container)
    NSLayoutConstraint.fillAndCenterInParent(view: container)

    container.addSubview(label)
    NSLayoutConstraint.fillAndCenterInParent(view: label, proportion: 0.9)
  }
}
