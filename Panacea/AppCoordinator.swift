//
//  AppCoordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/1/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

class AppCoordinator : Coordinator {
  var dismissCoordinator: (() -> Void)?

  private weak var window: UIWindow?
  private var placeDetailsCoordinator: PlaceDetailsCoordinator?
  private var tourDetailsCoordinator: TourDetailsCoordinator?

  init(_ window: UIWindow) {
    self.window = window

    self.window?.makeKeyAndVisible()
    self.window?.backgroundColor = UIColor.white
  }

  func start() {
    let mainTabBarController = HomeTabBarController()
    mainTabBarController.setupTabBarController()

    self.window?.rootViewController = mainTabBarController
  }

  func finish() {
    // Do nothing
  }

  func entityDetails(_ identifier: Int) {
    _ = PlaceDataHandler.shared.retrievePlaceInfo(with: identifier) { (_, entity) in
      guard let entity = entity, let controller = self.window?.rootViewController else {
        return
      }

      if entity.placeType == PlaceType.place {
        self.placeDetailsCoordinator = PlaceDetailsCoordinator(place: entity, navigator: controller)
        self.placeDetailsCoordinator?.start()
        self.placeDetailsCoordinator?.dismissCoordinator = self.onDismissPlaceDetailsCoordinator
      } else if entity.placeType == PlaceType.tour {
        self.tourDetailsCoordinator = TourDetailsCoordinator(tour: entity, navigator: controller)
        self.tourDetailsCoordinator?.start()
        self.tourDetailsCoordinator?.dismissCoordinator = self.onDismissTourDetailsCoordinator
      }
    }
  }

  func onDismissPlaceDetailsCoordinator() {
    self.placeDetailsCoordinator = nil
  }

  func onDismissTourDetailsCoordinator() {
    self.tourDetailsCoordinator = nil
  }
}
