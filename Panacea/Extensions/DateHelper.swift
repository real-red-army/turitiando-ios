//
//  DateHelper.swift
//  Panacea
//
//  Created by Luis Romero on 10/24/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

extension Date {
  private func compoment(component: Calendar.Component) -> Int {
    let calendar = Calendar.current

    return calendar.component(component, from: self)
  }

  var month: Int {
    return self.compoment(component: .month)
  }

  var currentDay: Int {
    return self.compoment(component: .day)
  }

  var year: Int {
    return self.compoment(component: .year)
  }

  /// Week day number 1, 2, 3, 4, 5, 6, 7 (Sabado = 7)
  var weekDay: Int {
    return self.compoment(component: .weekday)
  }

  /// Return day for a current date (Lunes, martes, miercoles, jueves, viernes, sabado, domingo)
  var weekDayStr: String {
    return WeekDays(rawValue: self.weekDay)?.day() ?? ""
  }
}
