//
//  UIImageHelper.swift
//  Panacea
//
//  Created by Luis Romero on 10/25/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import func AVFoundation.AVMakeRect

extension UIImage {
  static func optimizedImage(with name: String) -> UIImage? {
    guard let image = UIImage(named: name) else {
      return nil
    }

    return UIImage.optimizedImage(image)
  }

  static func resizedImage(image: UIImage, imageView: UIImageView) -> UIImage? {
    let renderer = UIGraphicsImageRenderer(size: imageView.bounds.size)

    let rect = AVMakeRect(aspectRatio: image.size, insideRect: imageView.bounds)

    return renderer.image { (_) in
      image.draw(in: CGRect(origin: .zero, size: rect.size))
    }
  }

  static func optimizedImage(_ image: UIImage?) -> UIImage? {
    guard let image = image else {
      return nil
    }

    let imageSize = image.size
    UIGraphicsBeginImageContextWithOptions(imageSize, true, 1.0)

    let result = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return result
  }
}
