//
//  DialogHelper.swift
//  Panacea
//
//  Created by Luis Romero on 3/15/20.
//  Copyright © 2020 blashadow. All rights reserved.
//

import UIKit

class DialogHelper {
  static func showMessage(controller: UIViewController, title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)

    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { _ in
    }))

    controller.present(alert, animated: true, completion: nil)
  }
}
