//
//  URLHelper.swift
//  Panacea
//
//  Created by Luis Romero on 2/5/20.
//  Copyright © 2020 blashadow. All rights reserved.
//

import UIKit

class URLHelper {
  static func openUrl(urlString: String) {
    guard let prospectUrl = URL(string: urlString) else {
        return
    }

    if UIApplication.shared.canOpenURL(prospectUrl) {
        UIApplication.shared.open(prospectUrl, options: [:], completionHandler: nil)
    }
  }

  static func openWhatApp(number: String, source: String) {
    let message = "Estoy interesado en este tour: \(source)"
    let cleanNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    let templateUrl = "https://api.whatsapp.com/send?phone=1\(cleanNumber)&text=\(message)"
    let internalUrl = "whatsapp://send?phone=1\(cleanNumber)&text=\(message)"

    let characterSet = CharacterSet.urlQueryAllowed
    let encodedUrl = templateUrl.addingPercentEncoding(withAllowedCharacters: characterSet) ?? ""
    let encodedInternalUrl = internalUrl.addingPercentEncoding(withAllowedCharacters: characterSet) ?? ""

    if let urlInstance = URL(string: encodedInternalUrl), UIApplication.shared.canOpenURL(urlInstance) {
      openUrl(urlString: encodedInternalUrl)
    } else {
      openUrl(urlString: encodedUrl)
    }
  }
}
