//
//  NSLayoutConstraintHelper.swift
//  Panacea
//
//  Created by Luis Romero on 2/5/20.
//  Copyright © 2020 blashadow. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
  static func fillAndCenterInParent(view: UIView, horizontalMargin: CGFloat = 0, proportion: CGFloat = 1) {
    guard let parent = view.superview else {
      return
    }

    NSLayoutConstraint.activate([
      view.widthAnchor.constraint(equalTo: parent.widthAnchor, multiplier: proportion, constant: -horizontalMargin),
      view.heightAnchor.constraint(equalTo: parent.heightAnchor, multiplier: proportion)
    ])

    NSLayoutConstraint.centerInParent(view: view)
  }

  static func fillAndCenterInScrollParent(view: UIView, horizontalMargin: CGFloat = 0, proportion: CGFloat = 1) {
    guard let parent = view.superview else {
      return
    }

    NSLayoutConstraint.activate([
      view.topAnchor.constraint(equalTo: parent.topAnchor),
      view.centerXAnchor.constraint(equalTo: parent.centerXAnchor),
      view.widthAnchor.constraint(equalTo: parent.widthAnchor, multiplier: proportion, constant: -horizontalMargin),
      view.heightAnchor.constraint(greaterThanOrEqualToConstant: 300),
      view.bottomAnchor.constraint(equalTo: parent.bottomAnchor)
    ])
  }

  static func centerInParent(view: UIView) {
    guard let parent = view.superview else {
      return
    }

    center(view: view, inView: parent)
  }

  static func center(view: UIView, inView: UIView) {
    NSLayoutConstraint.activate([
      view.centerYAnchor.constraint(equalTo: inView.centerYAnchor),
      view.centerXAnchor.constraint(equalTo: inView.centerXAnchor)
    ])
  }

  static func size(view: UIView, value: CGFloat) {
    size(view: view, width: value, height: value)
  }

  static func size(view: UIView, width: CGFloat, height: CGFloat) {
    NSLayoutConstraint.activate([
      view.widthAnchor.constraint(equalToConstant: width),
      view.heightAnchor.constraint(equalToConstant: height)
    ])
  }
}
