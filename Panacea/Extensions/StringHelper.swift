//
//  StringHelper.swift
//  Panacea
//
//  Created by Blashadow on 7/19/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

extension String {
  func capitalizingFirstLetter() -> String {
    return prefix(1).uppercased() + self.lowercased().dropFirst()
  }

  func renderHeight(contextWith itemWidth: CGFloat, font: UIFont) -> CGFloat {
    let attrs = [NSAttributedStringKey.font: font]
    let niceString = NSAttributedString(string: self, attributes: attrs as [NSAttributedStringKey : Any])
    let size = CGSize(width: itemWidth, height: 300)
    let rect = niceString.boundingRect(with: size, options: .usesLineFragmentOrigin, context: nil)

    return CGFloat(rect.size.height)
  }
}
