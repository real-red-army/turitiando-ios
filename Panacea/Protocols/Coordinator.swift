//
//  Coordinator.swift
//  Panacea
//
//  Created by Luis Romero on 11/1/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

protocol Coordinator {
  func start()

  var dismissCoordinator: (() -> Void)? { get set }
}
