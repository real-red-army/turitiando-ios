//
//  NavigationDelegate.swift
//  Panacea
//
//  Created by Luis Romero on 11/30/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

protocol NavigationDelegate: AnyObject {
  func dismiss()
}
