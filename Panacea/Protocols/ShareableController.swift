//
//  ShareableController.swift
//  Panacea
//
//  Created by Luis Romero on 11/18/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit

protocol ShareableController: UIViewController {
  func share(content: String)
}

extension ShareableController {
  func share(content: String) {
    // set up activity view controller
    let textToShare = [content]
    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
    activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

    // exclude some activity types from the list (optional)
    activityViewController.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.print]

    // present the view controller
    self.present(activityViewController, animated: true, completion: nil)
  }
}
