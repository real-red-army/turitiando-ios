//
//  AppDelegate.swift
//  Panacea
//
//  Created by Blashadow on 10/20/18.
//  Copyright © 2018 blashadow. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

import AppCenter
import AppCenterAnalytics
import AppCenterCrashes

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var appCoordinator: AppCoordinator?
  let notification = RemoteNotificationDelegate()
  let messaging = RemoteNotificationMessaging()

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    window = UIWindow(frame: UIScreen.main.bounds)

    if let window = window {
      appCoordinator = AppCoordinator(window)
      appCoordinator?.start()
    }

    // Google maps
    setupMaps()

    // Push notifications
    setupNotifications(application)

    // App center: Crash reporting and analitys usage
    setupAppCenter()

    return true
  }

  private func setupAppCenter() {
    MSAppCenter.start("104f7a25-e10c-40c2-85b2-0fcee07847f3", withServices:[
      MSAnalytics.self,
      MSCrashes.self
    ])
  }

  private func setupMaps() {
    /// Google maps
    GMSServices.provideAPIKey("AIzaSyCAy5tQ3_E6ZWh8-HFUVw1and_qS6hK0A8")
  }

  private func setupNotifications(_ application: UIApplication) {
    /// Push notification
    FirebaseApp.configure()

    /// For iOS 10 display notification (sent via APNS)
    UNUserNotificationCenter.current().delegate = notification

    /// For iOS 10 data message (sent via FCM)
    Messaging.messaging().delegate = messaging

    let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
    UNUserNotificationCenter.current().requestAuthorization(
      options: authOptions,
      completionHandler: {_, _ in })

    application.registerForRemoteNotifications()
  }

  func applicationWillResignActive(_ application: UIApplication) {
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
  }

  func applicationWillTerminate(_ application: UIApplication) {
  }

  public func application(_ application: UIApplication,
                          continue userActivity: NSUserActivity,
                          restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
      if let sourceUrl = userActivity.webpageURL {
        let items = sourceUrl.path.split(separator: "/")

        if items.count > 1, let entityId = Int(items[1]) {
          appCoordinator?.entityDetails(entityId)
        }
      }
      return true
  }

  /// Foreground notification
  func application(_ application: UIApplication,
                   didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                   fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    /// Completion handler call
    completionHandler(.newData)
  }
}
