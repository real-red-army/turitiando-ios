//
//  ListDataSource.swift
//  Panacea
//
//  Created by Luis Romero on 10/11/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class ListDataSource: NSObject {
  private var localItems: [SectionItems] = []

  var items: [SectionItems] {
    return localItems
  }

  init(_ items: [SectionItems]) {
    super.init()

    self.localItems = items
  }

  func setItems(_ items: [SectionItems]) {
    self.localItems = items
  }

  func updateItems(_ items: [SectionItems]) {
    self.localItems += items
  }
}
