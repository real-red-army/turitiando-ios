//
//  SectionDataSource.swift
//  Panacea
//
//  Created by Luis Romero on 10/11/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import IGListKit

class SectionDataSource: ListDataSource {
  private var sectionCellDisplayable: SectionCellDisplayable?

  init(_ sectionCellDisplayable: SectionCellDisplayable, items: [SectionItems]) {
    super.init(items)

    self.sectionCellDisplayable = sectionCellDisplayable
  }
}

extension SectionDataSource: ListAdapterDataSource {
  // MARK: - ListAdapterDataSource
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    return items
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    return SectionController(self.sectionCellDisplayable)
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    let view = EmptyCollectionViewCell(frame: .zero)
    view.setupDataCell()

    return view
  }
}
