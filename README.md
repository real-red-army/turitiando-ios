# Panacea
Turitiando iOS Client

### Project libraries
- Realm
- RxSwift
- Ruke
- IGListKit
- Skeleton
- SwiftLint
- LightBox
- Firebase
- GoogleMaps

# App Architecture
This app was build with the mvvm architecture design pattern in order to mitigate the known issues with the well know model view controller. Othe thing for this project is that we don't use storyboard to manage views for viewcontrollers.

## Architecture
https://medium.com/@alfianlosari/refactor-mvc-ios-app-to-mvvm-with-rxswift-in-minutes-alfian-losari-ec7905f946f4

#### Controller
Hold the instance elf the view and also the view model, it acts in the 	middle of everyone and interact with both of them, the viewModel update the modal and notify the controller to update the view

#### View
Do view stuff, create the view and theirs constraints 

#### ViewModel
- Hold the model object and update it when is required by the controller, this also means that the viewModel just interact with the model, but the http request must be performed by the controller it self and then send the update to the view model.
- All data manipulation and formatting stuff will be placed here

#### Model
- Hold the data for the controller

#### Coordinator
Manage every navigation stuffs from the controller to another controller


## List Architecture

#### SectionItems
This object hold all the items and the title for every section in a collection view, this item it's a diffable object that also contain a list of others items, theres a anum with associated values in order to support multiple objects.

#### Displayable Cell
This object got the job for sizing the cell and also deal with dequeue de cell and setup the cell.

#### SectionController
Object incharge of orchrestate the all collection view (ListSectionController object from IGListKit)

![List architecture diagram](/docs/images/ListStructure.png?raw=true "List Architecture")

[*All Diagrams*](https://whimsical.com/V8cfeDaGVy8qxFiiir1tao)

