//
//  NotificationViewController.swift
//  PanaceaNotificationContent
//
//  Created by Luis Romero on 10/18/19.
//  Copyright © 2019 blashadow. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {
  @IBOutlet var label: UILabel?
  override func viewDidLoad() {
    super.viewDidLoad()
  }
    
  func didReceive(_ notification: UNNotification) {
    self.label?.text = notification.request.content.body
  }
}
